call vendor\bin\doctrine.bat dbal:import tools/doctrineCreate.sql
call vendor\bin\doctrine.bat orm:convert-mapping --from-database --force annotation C:\wamp64\www\jarvis\entity
call php tools/doctrineClean.php
call vendor\bin\doctrine.bat orm:generate-entities --regenerate-entities=true --generate-annotations=true entity
call vendor\bin\doctrine.bat orm:generate-repositories repository
call vendor\bin\doctrine.bat orm:schema-tool:update --dump-sql
call php tools/doctrineCleanAfter.php