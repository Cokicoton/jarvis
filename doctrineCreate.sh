vendor/bin/doctrine dbal:import tools/doctrineCreate.sql
vendor/bin/doctrine orm:convert-mapping --from-database --force annotation entity
php tools/doctrineClean.php
vendor/bin/doctrine orm:generate-entities --regenerate-entities=true --generate-annotations=true entity
vendor/bin/doctrine orm:generate-repositories repository
vendor/bin/doctrine orm:schema-tool:update --dump-sql
php tools/doctrineCleanAfter.php