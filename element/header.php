<?php
include '../config/config.php';

require_once '../bootstrap.php';

//Voir si pas mieux dans chaque fichier
//Entities
require_once '../entity/Groups.php';
require_once '../entity/Jobs.php';
require_once '../entity/ParentJobs.php';
require_once '../entity/Roles.php';
require_once '../entity/Users.php';
require_once '../entity/Executions.php';
require_once '../entity/ExecutionCascades.php';
require_once '../entity/Planifications.php';
require_once '../entity/TypeLists.php';
require_once '../entity/Parameters.php';
require_once '../entity/ValueLists.php';
require_once '../entity/JobParams.php';
require_once '../entity/RoleRights.php';
require_once '../entity/UserRoles.php';
require_once '../entity/Colors.php';

//Repositories
require_once '../repository/GroupsRepository.php';
require_once '../repository/JobsRepository.php';
require_once '../repository/ParentJobsRepository.php';
require_once '../repository/RolesRepository.php';
require_once '../repository/UsersRepository.php';
require_once '../repository/ExecutionsRepository.php';
require_once '../repository/ExecutionCascadesRepository.php';
require_once '../repository/PlanificationsRepository.php';
require_once '../repository/TypeListsRepository.php';
require_once '../repository/ParametersRepository.php';
require_once '../repository/ValueListsRepository.php';
require_once '../repository/JobParamsRepository.php';
require_once '../repository/RoleRightsRepository.php';
require_once '../repository/UserRolesRepository.php';
require_once '../repository/ColorsRepository.php';

date_default_timezone_set($timezone);
switch($current){
  case "jobs":
         $title = $jobs_page_title;
         break;
  case "job":
         $title = $job_page_title;
         break;
  case "new_job":
         $title = $new_job_page_title;
         break;
  case "groups":
         $title = $groups_page_title;
         break;
  case "group":
         $title = $group_page_title;
         break;
  case "new_group":
         $title = $new_group_page_title;
         break;
  case "executions":
         $title = $executions_page_title;
         break;
  case "execution":
         $title = $execution_page_title;
         break;
  case "planifications":
         $title = $planifications_page_title;
         break;
  case "planification":
         $title = $planification_page_title;
         break;
  case "about":
         $title = $about_page_title;
         break;
  case "search":
         $title = $search_page_title;
         break;
  default:
         $title = $main_page_title;
         break;
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo $title; ?></title>
    <link rel="stylesheet" href="../css/bootstrap.min.css" media="screen">
    <link rel="stylesheet" href="../css/custom.css" media="screen">
  </head>
  <body>
    <?php include '../element/nav.php'; ?>
