    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
      <a class="navbar-brand" href="index.php"><?php echo $nav_application_name; ?></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarColor01">
        <ul class="navbar-nav mr-auto">
          <li class="<?php echo $current == 'index' ? 'nav-item active' : '' ?>">
            <a class="nav-link" href="index.php"><?php echo $nav_home; ?> <span class="sr-only">(current)</span></a>
          </li>
          <li class="<?php echo $current == 'groups' ? 'nav-item active' : '' ?>">
            <a class="nav-link" href="groups.php"><?php echo $nav_groups; ?></a>
          </li>
          <li class="<?php echo $current == 'jobs' ? 'nav-item active' : '' ?>">
            <a class="nav-link" href="jobs.php"><?php echo $nav_jobs; ?></a>
          </li>
          <li class="<?php echo $current == 'executions' ? 'nav-item active' : '' ?>">
            <a class="nav-link" href="executions.php"><?php echo $nav_executions; ?></a>
          </li>
          <li class="<?php echo $current == 'planifications' ? 'nav-item active' : '' ?>">
            <a class="nav-link" href="planifications.php"><?php echo $nav_planifications; ?></a>
          </li>
          <li class="<?php echo $current == 'about' ? 'nav-item active' : '' ?>">
            <a class="nav-link" href="about.php"><?php echo $nav_about; ?></a>
          </li>
        </ul>
        <form class="form-inline my-2 my-lg-0" action="search.php">
          <input class="form-control mr-sm-2" type="text" name="search" placeholder="<?php echo $nav_search_holder; ?>">
          <button class="btn btn-secondary my-2 my-sm-0" type="submit"><?php echo $nav_search; ?></button>
        </form>
      </div>
    </nav>
