function getIndexUrl() {
  if(document.location.href.lastIndexOf('.php') != -1) {
    return document.location.href.substring(0, document.location.href.lastIndexOf('/'));
  } else {
    if(document.location.href.substring(document.location.href.length-1) === '/'){
      return document.location.href.substring(0, document.location.href.length-1);
    } else {
      return document.location.href;
    }    
  }
}


var clickable = document.querySelectorAll('.clickable');
for(var i=0; i<clickable.length; i++){
  if(clickable[i].hasAttribute('data-type')){
    clickable[i].ondblclick = function(){
      document.location = getIndexUrl() + '/' + this.getAttribute('data-type') + '.php?id=' + this.getAttribute('data-id');
    }
  }
}

var addButton = document.querySelector('.add-button');
if(addButton !== null && typeof addButton !== 'undefined'){
  addButton.onclick = function(){
    if(this.getAttribute('data-page') === 'jobs'){
      document.location = getIndexUrl() + '/new_job.php';
    } else if(this.getAttribute('data-page') === 'groups') {
      document.location = getIndexUrl() + '/new_group.php';
    }
  }
}

