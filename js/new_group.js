var idJobs = [];
document.querySelector('#addJob').onclick = document.querySelector('#addGroupJobs').ondblclick = function() {
   var options = document.querySelectorAll('#addedGroupJobs option');   
   for(var i = 0; i<options.length; i++){
     if(options[i].value === "null" || options[i].id === document.querySelector('#addGroupJobs').value){
       document.querySelector('#addedGroupJobs').removeChild(options[i]);
       idJobs.splice(idJobs.indexOf(options[i].id), 1);
     }
   }

   var id = document.querySelector('#addGroupJobs').value;
   var name = '';
   var options = document.querySelectorAll('#addGroupJobs option');
   for(var i = 0; i<options.length; i++){
     if(options[i].value === id){
       name = options[i].textContent;
     }
   }
  var option = document.createElement('option');
  option.id = id;
  option.textContent = name;
  document.querySelector('#addedGroupJobs').appendChild(option);
  idJobs.push(id);
  var res = '';
  for(var i=0; i<idJobs.length; i++){
    res = res + id + '_';
  }
  res = res.substring(0, res.length-1);
  document.querySelector('#idJobs').value = idJobs;
}
var colors = document.querySelectorAll('#colorSelect button');
for(var i=0; i<colors.length; i++){
  colors[i].onclick = function(){
    document.querySelector('#color').value = this.getAttribute('data-value');
  }
}

document.querySelector('#addGroupName').onchange = function(){
  if(this.value !== '' && document.querySelector('#addGroupDescription').value !== ''){
    document.querySelector('#submitAddGroup').disabled = false;
    document.querySelector('').removeAttribute('disabled');
  } else {
    document.querySelector('#submitAddGroup').disabled = true;
  }
}

document.querySelector('#addGroupDescription').onchange = function(){
  if(this.value !== '' && document.querySelector('#addGroupName').value !== ''){
    document.querySelector('#submitAddGroup').disabled = false;
    document.querySelector('').removeAttribute('disabled');
  } else {
    document.querySelector('#submitAddGroup').disabled = true;
  }
}
