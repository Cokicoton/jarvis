document.querySelector('#addJobName').onchange = function(){
  if(this.value !== '' && document.querySelector('#addJobDescription').value !== '' && document.querySelector('#addScriptName').value !== ''){
    document.querySelector('#submitAddJob').disabled = false;
    document.querySelector('').removeAttribute('disabled');
  } else {
    document.querySelector('#submitAddJob').disabled = true;
  }
}

document.querySelector('#addJobDescription').onchange = function(){
  if(this.value !== '' && document.querySelector('#addJobName').value !== '' && document.querySelector('#addScriptName').value !== ''){
    document.querySelector('#submitAddJob').disabled = false;
    document.querySelector('').removeAttribute('disabled');
  } else {
    document.querySelector('#submitAddJob').disabled = true;
  }
}

document.querySelector('#addScriptName').onchange = function(){
  if(this.value !== '' && document.querySelector('#addJobName').value !== '' && document.querySelector('#addJobDescription').value !== ''){
    document.querySelector('#submitAddJob').disabled = false;
    document.querySelector('').removeAttribute('disabled');
  } else {
    document.querySelector('#submitAddJob').disabled = true;
  }
}

document.querySelector('#addParameter').onclick = function(){
 var name = document.querySelector('#addParamName').value;
 var description = document.querySelector('#addParamDescription').value;
 var defaultValue = document.querySelector('#addParamDefault').value;
 if(name !== ""){
     if(document.querySelector('#addParamType').options[document.querySelector('#addParamType').selectedIndex].text != 'select'){
     var button = document.createElement('button');
     button.classList.add('btn');
     button.classList.add('btn-primary');
     button.classList.add('d-inline');
     button.classList.add('m-1');
     button.textContent = name + ' / ' + document.querySelector('#addParamType').options[document.querySelector('#addParamType').selectedIndex].text;
     var listParams = document.querySelector('#params').value;
     listParams = listParams + ';&[&' + name + '<<>>' + description + '<<>>' + defaultValue + '&_&' + document.querySelector('#addParamType').value + '&]&';
     document.querySelector('#params').value = listParams;
     document.querySelector('#paramsList').appendChild(button);
     document.querySelector('#addParamName').value = '';
     document.querySelector('#addParamDescription').value = '';
     document.querySelector('#addParamDefault').value = '';
   } else {
     document.querySelector('#selectModal').style.display = 'block';
     document.querySelector('#selectModal .close').onclick = function(){
       document.querySelector('#selectModal').style.display = 'none';
     }
     document.querySelector('#saveSelectModal').onclick = function(){
       var list = document.querySelector('#addJobModalSelectList').value;
       if(list !== ''){
         document.querySelector('#selectValues').value = document.querySelector('#selectValues').value + '&[&' + document.querySelector('#addParamName').value + '&_&' + list.replace(/\n/g,"&,&") + '&]&';
         var button = document.createElement('button');
         button.classList.add('btn');
         button.classList.add('btn-primary');
         button.classList.add('d-inline');
         button.classList.add('m-1');
         button.textContent = document.querySelector('#addParamName').value + ' / select';
         var listParams = document.querySelector('#params').value;
         listParams = listParams + ';&[&' + name + '<<>>' + description + '<<>>' + defaultValue + '&_&' + document.querySelector('#addParamType').value + '&]&';
         document.querySelector('#params').value = listParams;
         document.querySelector('#paramsList').appendChild(button);
         document.querySelector('#addParamName').value = '';
         document.querySelector('#selectModal').style.display = 'none';
         document.querySelector('#addJobModalSelectList').value = '';
         document.querySelector('#addParamDescription').value = '';
         document.querySelector('#addParamDefault').value = '';
       }
     }
   }
 } 
}
