function drawBars(periods, success, fails, running, duration) {
  var barChartData = {
        labels: periods,
	datasets: [{
		label: 'Echecs',
		backgroundColor: window.chartColors.red,
		data: fails
	}, {
		label: 'Succès',
		backgroundColor: window.chartColors.blue,
		data: success
	}, {
		label: 'En cours',
		backgroundColor: window.chartColors.green,
		data: running
	}]

  };
  switch(duration) {
    case 'total':
      var titre = 'Total des executions';
      break;
    case 'an':
      var titre = 'Executions sur les 12 derniers mois';
      break;
    case 'mois':
      var titre = 'Executions sur les 30 derniers jours';
      break;
    case 'semaine':
      var titre = 'Executions sur les 7 derniers jours';
      break;
  }
  var ctx = document.getElementById('canvas').getContext('2d');
        if(window.myBar !== null && typeof window.myBar !== 'undefined'){
          window.myBar.destroy();
        }        
	window.myBar = new Chart(ctx, {
		type: 'bar',
		data: barChartData,
		options: {
			title: {
				display: true,
				text: titre
			},
			tooltips: {
				mode: 'index',
				intersect: false
			},
			responsive: true,
			scales: {
				xAxes: [{
					stacked: true,
				}],
				yAxes: [{
					stacked: true
				}]
			}
		}
	});
}
var actuel = null;
window.onload = function() {
	drawBars(periodTotal, successTotal, failTotal, runningTotal, 'total');
};

var periodButtons = document.querySelectorAll('#periodSelection button');
for(var i=0; i<periodButtons.length; i++){
  periodButtons[i].onclick = function(){    
    switch(this.id) {
      case 'total':
        drawBars(periodTotal, successTotal, failTotal, runningTotal, this.id);
        break;
      case 'an':
        drawBars(periodYear, successYear, failYear, runningYear, this.id);
        break;
      case 'mois':
        drawBars(periodMonth, successMonth, failMonth, runningMonth, this.id);
        break;
      case 'semaine':
        drawBars(periodWeek, successWeek, failWeek, runningWeek, this.id);
        break;
    }
  }
}
