# parser

## Une web app simple de gestion de jobs (script système) nécessitant une configuration PHP - MariaDb (ou MySql)

![capture](jarvis_home.PNG)

![capture](jarvis_job.PNG)

![capture](chartExecutions.PNG)

## Paramétrage
Paramètrer la base de données:
<pre><code>Setup doctrine in Bootstrap.php
	$dbParams = array(
	    'driver' => 'pdo_mysql',
	    'host' => 'your_host',
	    'user' => 'user_login',
	    'password' => 'user_password',
	    'dbname' => 'database_name',
	);</code></pre>

Initaliser doctrine
<pre><code>doctrineCreate.bat</code></pre>

Réinitialiser doctrine
<pre><code>doctrineDelete.bat</code></pre>
puis
<pre><code>doctrineCreate.bat</code></pre>


Theme Bootstrap : https://bootswatch.com/cyborg/

Si besoin installer les dépendances:
<pre><code>composer install</code></pre>

## Utilisation
A la création d'un job, le script doit être saisi avec son chemin.

![capture](scriptChoice.PNG)

Ce script doit renvoyer lorsqu'on l'appele le type de log (file ou url) suivi par espace et par le fichier ou l'url de log.
Ce fichier ou cet url doit être accessible au serveur via un file_get_contents et retourner une dernière ligne
- end(0) en cas de succès du job
- end(1) en cas d'echec du job
