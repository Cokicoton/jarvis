<?php
/** 
  * Cleaning the order columns name in Groups
**/
$content = file_get_contents(__dir__.'/../entity/Groups.php');
$newContent = str_replace('     * @ORM\Column(name="order", type="smallint", precision=0, scale=0, nullable=true, unique=false)', '     * @ORM\Column(name="`order`", type="smallint", precision=0, scale=0, nullable=true, unique=false)', $content);
file_put_contents(__dir__.'/../entity/Groups.php', $newContent);

copy(__dir__.'/../tools/ExecutionsRepositoryCustom.php', __dir__.'/../repository/ExecutionsRepository.php');

?>
