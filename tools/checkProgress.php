<?php 
  include '../config/config.php';
  include '../lang/' . $lang . '.php';
  $current = '';
  require_once '../bootstrap.php';

  //Entities
  require_once '../entity/Groups.php';
  require_once '../entity/Jobs.php';
  require_once '../entity/Executions.php';
  require_once '../entity/Planifications.php';
  require_once '../entity/Users.php';

  //Repositories
  require_once '../repository/JobsRepository.php';
  require_once '../repository/ExecutionsRepository.php';
  require_once '../repository/PlanificationsRepository.php';

  date_default_timezone_set($timezone);


  $executions = $entityManager->getRepository('Executions')->findBy( array('status' => '-1'));
  foreach($executions as $execution){
    if(!is_null($execution->getLog())){
    if($execution->getTypeLog() == 'file'){
      $tab = file($execution->getLog());
      $lastLine = $tab[count($tab)-1];
    } else {
      $content = file_get_contents($execution->getLog());
      $lastLine = substr($content, -7);
    }
    if(substr($lastLine, 0, 3) == 'end') {
      $date = new DateTime();
      if($execution->getTypeLog() == 'file'){
        $date->setTimestamp(filemtime($execution->getLog()));
      }
      $execution->setEndDate($date);
      if(substr($lastLine, 0, 6) == 'end(0)') { //success
        $execution->setStatus(1);
      } else { //error
        $execution->setStatus(0);
      }
    }
    $execution->setLogDetails(file_get_contents($execution->getLog()));
    $entityManager->persist($execution);
    $entityManager->flush();
    }
  }

  $planifications = $entityManager->getRepository('Planifications')->findBy( array('status' => '1'));
  foreach($planifications as $planification){
    $now = new DateTime();
    if(!is_null($planification->getEndDate()) && $planification->getEndDate() < $now){
      $planification->setStatus(0);
      $entityManager->persist($planification);
      $entityManager->flush();
    } elseif($planification->getNextTerm() < $now){
      //Appel 
      $params = json_decode(str_replace(',}', '}', $planification->getParams()));
      $urlParams = $home_url . (substr($home_url, -1) == '/' ? '' : '/') . 'run.php?';
      $urlParams = $urlParams . 'id=' . $planification->getJob()->getId();
      $urlParams = $urlParams . '&PlanificationStartDate=&';
      $urlParams = $urlParams . 'PlanificationFrequency=&';
      $urlParams = $urlParams . 'PlanificationEndDate=&';
      foreach($params as $paramName => $paramValue) {
        $urlParams = $urlParams . '&' . urlencode($paramName) . '=' . urlencode($paramValue);
      }
      if(!is_null($planification->getFrequencyTime())){
        $h = intval($planification->getFrequencyTime()->format('H'));
        $m = intval($planification->getFrequencyTime()->format('i'));
        $interval = new DateInterval('PT' . $h . 'H' . $m . 'M0S');
        $temp = clone $planification->getNextTerm();
        $temp = $temp->add($interval);
        if(!is_null($planification->getFrequencyDay())){
          $interval = new DateInterval('P' . $planification->getFrequencyDay() . 'D');
          $temp = $temp->add($interval);
        }
        $planification->setNextTerm($temp);
      } else {
        $planification->setStatus(0);
      }
      $arrContextOptions=array(
          "ssl"=>array(
              "verify_peer"=>false,
              "verify_peer_name"=>false,
          ),
      );  

      $response = file_get_contents($urlParams, false, stream_context_create($arrContextOptions));
      $entityManager->persist($planification);
      $entityManager->flush();
    }
  }
?>
