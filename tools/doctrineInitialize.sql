/* Groups */

INSERT INTO `groups`(`name`, `description`, `color`, `order`) VALUES ('Groupe 1', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'bg-primary', 1);

INSERT INTO `groups`(`name`, `description`, `color`, `order`) VALUES ('Groupe 2', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here', 'bg-secondary', 2);

INSERT INTO `groups`(`name`, `description`, `color`, `order`) VALUES ('Groupe 3', 'If you use this site regularly and would like to help keep the site on the Internet, please consider donating a small sum to help pay for the hosting and bandwidth bill. There is no minimum donation, any sum is appreciated - click here to donate using PayPal. Thank you for your support.', null, 1);

INSERT INTO `groups`(`name`, `description`, `color`, `order`) VALUES ('Groupe 4', 'Descrition groupe 4', 'bg-info', 1);

/* Jobs */

INSERT INTO `jobs`(`name`, `group_id`, `group_order`, `script`, `description`, `last_exec`) VALUES ('Job 1', 1, 1, 'C:\\wamp64\\www\\JEKYLL\\test\\script1.bat', 'Script de sauvegarde de la base truc', '2019-10-23 06:11:07');

INSERT INTO `jobs`(`name`, `group_id`, `group_order`, `script`, `description`, `last_exec`) VALUES ('Job 2', 1, 2, 'del_truc', 'Script de suppression de la base truc', null);

INSERT INTO `jobs`(`name`, `group_id`, `group_order`, `script`, `description`, `last_exec`) VALUES ('Job 3', 2, 1, 'machin 3', 'Script de machin 3', '2019-09-12 16:33:54');

INSERT INTO `jobs`(`name`, `group_id`, `group_order`, `script`, `description`, `last_exec`) VALUES ('Job 4', 3, 1, 'machin 4', 'Script de machin 4', null);

INSERT INTO `jobs`(`name`, `group_id`, `group_order`, `script`, `description`, `last_exec`) VALUES ('Job 5', null, null, 'script5', 'Lance deux sous-jobs', null);

INSERT INTO `jobs`(`name`, `group_id`, `group_order`, `script`, `description`, `last_exec`) VALUES ('Job 6', null, null, 'machin 6', 'Script de machin 6', null);

INSERT INTO `jobs`(`name`, `group_id`, `group_order`, `script`, `description`, `last_exec`) VALUES ('Job 7', null, null, 'machin 7', 'Script de machin 7', null);

/* Parent_jobs */

INSERT INTO `parent_jobs`(`parent_id`, `child_id`, `child_order`) VALUES (5, 1, 1);
INSERT INTO `parent_jobs`(`parent_id`, `child_id`, `child_order`) VALUES (5, 3, 2);

/* Users */

INSERT INTO `users`(`name`, `password`, `salt`, `status`, `nb_attempts`) VALUES ('Coki', '2470424ed406d30833ce526901145152', '8VnuPXWjfg1MztBJOr9F', 1, 0);


/* Executions */

INSERT INTO `executions`(`name`, `job_id`, `user_id`, `start_date`, `end_date`, `status`, `params`, `log`, `type_log`, `log_details`) VALUES ('exec_1_20191023061107', 1, 1, '2019-10-23 06:11:07', '2019-10-23 10:30:45', 1, '{}', 'logs success', 'file', null);

INSERT INTO `executions`(`name`, `job_id`, `user_id`, `start_date`, `end_date`, `status`, `params`, `log`, `type_log`, `log_details`) VALUES ('exec_3_20190912163354', 3, 1, '2019-09-12 16:33:54', '2019-09-13 00:12:05', 0, '{}', 'logs error', 'file', null);

/* Roles */

INSERT INTO `roles`(`name`, `description`) VALUES ('administrateur', 'Droits administrateur');

/* Role_rights */

INSERT INTO `role_rights`(`name`, `job_id`, `group_id`, `role_id`) VALUES ('right_administrateur_job_1', 1, null, 1);
INSERT INTO `role_rights`(`name`, `job_id`, `group_id`, `role_id`) VALUES ('right_administrateur_job_3', 3, null, 1);
INSERT INTO `role_rights`(`name`, `job_id`, `group_id`, `role_id`) VALUES ('right_administrateur_job_1', null, 1, 1);

/* User_roles */

INSERT INTO `user_roles`(`name`, `user_id`, `role_id`) VALUES ('Coki_administrateur', 1, 1);

/* Type_lists */

INSERT INTO `type_lists`(`name`, `description`, `order`, `enabled`) VALUES ('text', 'desciption', 1, 1);
INSERT INTO `type_lists`(`name`, `description`, `order`, `enabled`) VALUES ('select', 'desciption', 2, 1);
INSERT INTO `type_lists`(`name`, `description`, `order`, `enabled`) VALUES ('checkbox', 'desciption', 3, 1);
INSERT INTO `type_lists`(`name`, `description`, `order`, `enabled`) VALUES ('date', 'desciption', 4, 1);
INSERT INTO `type_lists`(`name`, `description`, `order`, `enabled`) VALUES ('datetime-local', 'desciption', 5, 1);
INSERT INTO `type_lists`(`name`, `description`, `order`, `enabled`) VALUES ('time', 'desciption', 6, 1);
INSERT INTO `type_lists`(`name`, `description`, `order`, `enabled`) VALUES ('number', 'desciption', 7, 1);
INSERT INTO `type_lists`(`name`, `description`, `order`, `enabled`) VALUES ('email', 'desciption', 8, 1);
INSERT INTO `type_lists`(`name`, `description`, `order`, `enabled`) VALUES ('password', 'desciption', 9, 1);
INSERT INTO `type_lists`(`name`, `description`, `order`, `enabled`) VALUES ('url', 'desciption', 10, 1);
INSERT INTO `type_lists`(`name`, `description`, `order`, `enabled`) VALUES ('radio', 'desciption', 11, 0);
INSERT INTO `type_lists`(`name`, `description`, `order`, `enabled`) VALUES ('button', 'desciption', 12, 0);
INSERT INTO `type_lists`(`name`, `description`, `order`, `enabled`) VALUES ('color', 'desciption', 13, 0);
INSERT INTO `type_lists`(`name`, `description`, `order`, `enabled`) VALUES ('file', 'desciption', 14, 0);
INSERT INTO `type_lists`(`name`, `description`, `order`, `enabled`) VALUES ('hidden', 'desciption', 15, 0);
INSERT INTO `type_lists`(`name`, `description`, `order`, `enabled`) VALUES ('image', 'desciption', 16, 0);
INSERT INTO `type_lists`(`name`, `description`, `order`, `enabled`) VALUES ('month', 'desciption', 17, 0);
INSERT INTO `type_lists`(`name`, `description`, `order`, `enabled`) VALUES ('range', 'desciption', 18, 0);
INSERT INTO `type_lists`(`name`, `description`, `order`, `enabled`) VALUES ('reset', 'desciption', 19, 0);
INSERT INTO `type_lists`(`name`, `description`, `order`, `enabled`) VALUES ('search', 'desciption', 20, 0);
INSERT INTO `type_lists`(`name`, `description`, `order`, `enabled`) VALUES ('submit', 'desciption', 21, 0);
INSERT INTO `type_lists`(`name`, `description`, `order`, `enabled`) VALUES ('tel', 'desciption', 22, 0);
INSERT INTO `type_lists`(`name`, `description`, `order`, `enabled`) VALUES ('week', 'desciption', 23, 0);

/* Parameters */

INSERT INTO `parameters`(`name`, `type_id`, `size`, `description`) VALUES ('Base', 23, null, 'Choix de la base de sauvegarde');
INSERT INTO `parameters`(`name`, `type_id`, `size`, `description`) VALUES ('Nom fichier sauvegarde', 19, null, 'Nom du fichier de sauvegarde');
INSERT INTO `parameters`(`name`, `type_id`, `size`, `description`) VALUES ('Date', 5, null, 'Date de l''extraction');

/* Job_params */

INSERT INTO `job_params`(`name`, `job_id`, `param_id`, `default_value`, `description`) VALUES ('Base', 1, 1, null, 'Choix de la base de sauvegarde');
INSERT INTO `job_params`(`name`, `job_id`, `param_id`, `default_value`, `description`) VALUES ('Nom fichier sauvegarde', 1, 2, 'sauvegarde_[date]', 'Nom du fichier de sauvegarde');
INSERT INTO `job_params`(`name`, `job_id`, `param_id`, `default_value`, `description`) VALUES ('Date', 3, 3, localtime, 'Date de l''extraction');

/* Value_lists */

INSERT INTO `value_lists`(`param_id`, `value`, `description`) VALUES (1, 'RCT', 'Base RCT');
INSERT INTO `value_lists`(`param_id`, `value`, `description`) VALUES (1, 'RCTMIG', 'Base RCTMIG');
INSERT INTO `value_lists`(`param_id`, `value`, `description`) VALUES (1, 'TST', 'Base TST');


