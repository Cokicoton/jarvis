<?php
/** 
  * Updating foreign keys constraints not nullable
**/
$entities = array_diff(scandir(__dir__.'/../entity'), array('..', '.', 'Jobs.php', 'RoleRights.php'));
foreach($entities as $entity){
  $content = file_get_contents(__dir__.'/../entity/'.$entity);
  if(count(explode('@ORM\JoinColumn(', $content)) > 1){
    $newContent = preg_replace('/referencedColumnName="id"/', 'referencedColumnName="id", nullable=false', $content);
    file_put_contents(__dir__.'/../entity/'.$entity, $newContent);
  }  
}

$content = file_get_contents(__dir__.'/../entity/RoleRights.php');
$newContent = preg_replace('/name="role_id"\, referencedColumnName="id"\)/', 'name="role_id", referencedColumnName="id", nullable=false)', $content);
file_put_contents(__dir__.'/../entity/RoleRights.php', $newContent);

/** 
  * Adding Repository classes
**/
$entities = array_diff(scandir(__dir__.'/../entity'), array('..', '.'));
foreach($entities as $entity){
  $content = file_get_contents(__dir__.'/../entity/'.$entity);
  $entityName = explode('.php', $entity)[0];
  $newContent = str_replace('* @ORM\Entity', '* @ORM\Entity(repositoryClass="' . $entityName . 'Repository")', $content);
  file_put_contents(__dir__.'/../entity/'.$entity, $newContent);
}

/** 
  * Cleaning Execution_cascades entity
**/
$content = file_get_contents(__dir__.'/../entity/ExecutionCascades.php');
$newContent = str_replace('     *   @ORM\JoinColumn(name="execution_id", referencedColumnName="id", nullable=false)', '     *   @ORM\JoinColumn(name="execution_id", referencedColumnName="id", nullable=true)', $content);
file_put_contents(__dir__.'/../entity/ExecutionCascades.php', $newContent);

?>
