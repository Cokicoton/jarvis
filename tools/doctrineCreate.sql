CREATE TABLE Groups (
    id SMALLINT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(40) NOT NULL,
    description TEXT,
	color VARCHAR(20),
	`order` SMALLINT
)
ENGINE=InnoDB;


CREATE TABLE Jobs (
    id SMALLINT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(40) NOT NULL,
    group_id SMALLINT,
    group_order SMALLINT,
    script VARCHAR(255),
    description TEXT,
	last_exec DATETIME,
    CONSTRAINT ct_group_id
        FOREIGN KEY (group_id)
        REFERENCES Groups(id)
)
ENGINE=InnoDB;

CREATE TABLE Parent_jobs (
    id SMALLINT AUTO_INCREMENT PRIMARY KEY,
    parent_id SMALLINT NOT NULL,
    child_id SMALLINT NOT NULL,
    child_order SMALLINT,
    CONSTRAINT ct_parent_id
        FOREIGN KEY (parent_id)
        REFERENCES Jobs(id),
    CONSTRAINT ct_child_id
        FOREIGN KEY (child_id)
        REFERENCES Jobs(id)
)
ENGINE=InnoDB;

CREATE TABLE Type_lists (
    id SMALLINT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(40) NOT NULL,
    description TEXT,
    `order` SMALLINT,
    enabled BOOL
)
ENGINE=InnoDB;


CREATE TABLE Parameters (
    id SMALLINT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(40) NOT NULL,
    type_id SMALLINT NOT NULL,
    size SMALLINT,
    description TEXT,
    CONSTRAINT ct_type_id
        FOREIGN KEY (type_id)
        REFERENCES Type_lists(id)
)
ENGINE=InnoDB;

CREATE TABLE Value_lists (
    id SMALLINT AUTO_INCREMENT PRIMARY KEY,
    param_id SMALLINT NOT NULL,
    value VARCHAR(40) NOT NULL,
    description TEXT,
    CONSTRAINT ct_value_param_id
        FOREIGN KEY (param_id)
        REFERENCES Parameters(id)
)
ENGINE=InnoDB;


CREATE TABLE Job_params (
    id SMALLINT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(40) NOT NULL,
    job_id SMALLINT NOT NULL,
    param_id SMALLINT NOT NULL,
    default_value VARCHAR(255),
    description TEXT,
    CONSTRAINT ct_job_id
        FOREIGN KEY (job_id)
        REFERENCES Jobs(id),
    CONSTRAINT ct_param_id
        FOREIGN KEY (param_id)
        REFERENCES Parameters(id)
)
ENGINE=InnoDB;

CREATE TABLE Users (
    id SMALLINT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(40) NOT NULL,
    password VARCHAR(64) NOT NULL,
    salt VARCHAR(20) NOT NULL,
    status SMALLINT NOT NULL,
    nb_attempts SMALLINT
)
ENGINE=InnoDB;


CREATE TABLE Executions (
    id SMALLINT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(40) NOT NULL,
    job_id SMALLINT NOT NULL,
    user_id SMALLINT NOT NULL,
    start_date DATETIME NOT NULL,
    end_date DATETIME ,
    status SMALLINT NOT NULL,
	params TEXT NOT NULL,
    log TEXT,
	type_log VARCHAR(40),
    log_details TEXT,
    CONSTRAINT ct_execution_job_id
        FOREIGN KEY (job_id)
        REFERENCES Jobs(id),
    CONSTRAINT ct_user_id
        FOREIGN KEY (user_id)
        REFERENCES Users(id)
)
ENGINE=InnoDB;

CREATE TABLE Execution_cascades (
    id SMALLINT AUTO_INCREMENT PRIMARY KEY,
    execution_parent_id SMALLINT NOT NULL,
    job_id SMALLINT NOT NULL,
    execution_id SMALLINT,
    execution_order SMALLINT,
    CONSTRAINT ct_execution_multi_execution_parent_id
        FOREIGN KEY (execution_parent_id)
        REFERENCES Executions(id),
    CONSTRAINT ct_execution_multi_job_id
        FOREIGN KEY (job_id)
        REFERENCES Jobs(id),
    CONSTRAINT ct_execution_multi_execution_id
        FOREIGN KEY (execution_id)
        REFERENCES Executions(id)
)
ENGINE=InnoDB;

CREATE TABLE Planifications (
    id SMALLINT AUTO_INCREMENT PRIMARY KEY,
    job_id SMALLINT NOT NULL,
    user_id SMALLINT NOT NULL,
    start_date DATETIME NOT NULL,
    end_date DATETIME,
    frequencyTime TIME,
    frequencyDay INT,
    next_term DATETIME,
    params TEXT NOT NULL,
    status SMALLINT NOT NULL,
    CONSTRAINT ct_planification_job_id
        FOREIGN KEY (job_id)
        REFERENCES Jobs(id),
    CONSTRAINT ct_planification_user_id
        FOREIGN KEY (user_id)
        REFERENCES Users(id)
)
ENGINE=InnoDB;

CREATE TABLE Roles (
    id SMALLINT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(40) NOT NULL,
    description TEXT
)
ENGINE=InnoDB;


CREATE TABLE User_roles (
    id SMALLINT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(40) NOT NULL,
    user_id SMALLINT NOT NULL,
    role_id SMALLINT NOT NULL,
    CONSTRAINT ct_user_role_id
        FOREIGN KEY (user_id)
        REFERENCES Users(id),
    CONSTRAINT ct_role_id
        FOREIGN KEY (role_id)
        REFERENCES Roles(id)
)
ENGINE=InnoDB;

CREATE TABLE Role_rights (
    id SMALLINT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(40) NOT NULL,
    job_id SMALLINT,
    group_id SMALLINT,
    role_id SMALLINT NOT NULL,
    CONSTRAINT ct_role_right_id
        FOREIGN KEY (role_id)
        REFERENCES Roles(id),
    CONSTRAINT ct_role_right_group_id
        FOREIGN KEY (group_id)
        REFERENCES Groups(id),
    CONSTRAINT ct_role_right_job_id
        FOREIGN KEY (job_id)
        REFERENCES Jobs(id)
)
ENGINE=InnoDB;

CREATE TABLE Colors (
    id SMALLINT AUTO_INCREMENT PRIMARY KEY,
    class VARCHAR(40) NOT NULL
)
ENGINE=InnoDB;

/* Colors */

INSERT INTO `Colors`(`class`) VALUES ('bg-primary');
INSERT INTO `Colors`(`class`) VALUES ('bg-secondary');
INSERT INTO `Colors`(`class`) VALUES ('bg-success');
INSERT INTO `Colors`(`class`) VALUES ('bg-danger');
INSERT INTO `Colors`(`class`) VALUES ('bg-warning');
INSERT INTO `Colors`(`class`) VALUES ('bg-info');
INSERT INTO `Colors`(`class`) VALUES ('bg-light');
INSERT INTO `Colors`(`class`) VALUES ('bg-dark');
INSERT INTO `Colors`(`class`) VALUES ('bg-white');


delimiter |
CREATE TRIGGER job_group_only_insert AFTER INSERT ON Role_rights
FOR EACH ROW 
BEGIN 
    IF(
        (NEW.job_id IS NULL AND NEW.group_id IS NULL) OR 
        (NEW.job_id IS NOT NULL AND NEW.group_id IS NOT NULL)
    )
    THEN
        SIGNAL SQLSTATE '44000'
            SET MESSAGE_TEXT = 'check constraint job_group_only_insert failed';
    END IF;
END;|
delimiter ;

delimiter |
CREATE TRIGGER job_group_only_update AFTER UPDATE ON Role_rights
FOR EACH ROW 
BEGIN 
    IF(
        (NEW.job_id IS NULL AND NEW.group_id IS NULL) OR 
        (NEW.job_id IS NOT NULL AND NEW.group_id IS NOT NULL)
    )
    THEN
        SIGNAL SQLSTATE '44000'
            SET MESSAGE_TEXT = 'check constraint job_group_only_update failed';
    END IF;
END;|
delimiter ;
