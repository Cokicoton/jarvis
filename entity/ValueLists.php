<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * ValueLists
 *
 * @ORM\Table(name="Value_lists", indexes={@ORM\Index(name="ct_value_param_id", columns={"param_id"})})
 * @ORM\Entity(repositoryClass="ValueListsRepository")
 */
class ValueLists
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="smallint", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=40, precision=0, scale=0, nullable=false, unique=false)
     */
    private $value;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=65535, precision=0, scale=0, nullable=true, unique=false)
     */
    private $description;

    /**
     * @var \Parameters
     *
     * @ORM\ManyToOne(targetEntity="Parameters")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="param_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $param;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value.
     *
     * @param string $value
     *
     * @return ValueLists
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value.
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return ValueLists
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set param.
     *
     * @param \Parameters $param
     *
     * @return ValueLists
     */
    public function setParam(\Parameters $param)
    {
        $this->param = $param;

        return $this;
    }

    /**
     * Get param.
     *
     * @return \Parameters
     */
    public function getParam()
    {
        return $this->param;
    }
}
