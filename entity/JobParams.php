<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * JobParams
 *
 * @ORM\Table(name="Job_params", indexes={@ORM\Index(name="ct_param_id", columns={"param_id"}), @ORM\Index(name="ct_job_id", columns={"job_id"})})
 * @ORM\Entity(repositoryClass="JobParamsRepository")
 */
class JobParams
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="smallint", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=40, precision=0, scale=0, nullable=false, unique=false)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="default_value", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $defaultValue;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=65535, precision=0, scale=0, nullable=true, unique=false)
     */
    private $description;

    /**
     * @var \Jobs
     *
     * @ORM\ManyToOne(targetEntity="Jobs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="job_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $job;

    /**
     * @var \Parameters
     *
     * @ORM\ManyToOne(targetEntity="Parameters")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="param_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $param;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return JobParams
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set defaultValue.
     *
     * @param string|null $defaultValue
     *
     * @return JobParams
     */
    public function setDefaultValue($defaultValue = null)
    {
        $this->defaultValue = $defaultValue;

        return $this;
    }

    /**
     * Get defaultValue.
     *
     * @return string|null
     */
    public function getDefaultValue()
    {
        return $this->defaultValue;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return JobParams
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set job.
     *
     * @param \Jobs $job
     *
     * @return JobParams
     */
    public function setJob(\Jobs $job)
    {
        $this->job = $job;

        return $this;
    }

    /**
     * Get job.
     *
     * @return \Jobs
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * Set param.
     *
     * @param \Parameters $param
     *
     * @return JobParams
     */
    public function setParam(\Parameters $param)
    {
        $this->param = $param;

        return $this;
    }

    /**
     * Get param.
     *
     * @return \Parameters
     */
    public function getParam()
    {
        return $this->param;
    }
}
