<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Executions
 *
 * @ORM\Table(name="Executions", indexes={@ORM\Index(name="ct_user_id", columns={"user_id"}), @ORM\Index(name="ct_execution_job_id", columns={"job_id"})})
 * @ORM\Entity(repositoryClass="ExecutionsRepository")
 */
class Executions
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="smallint", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=40, precision=0, scale=0, nullable=false, unique=false)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $startDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="end_date", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $endDate;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint", precision=0, scale=0, nullable=false, unique=false)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="params", type="text", length=65535, precision=0, scale=0, nullable=false, unique=false)
     */
    private $params;

    /**
     * @var string|null
     *
     * @ORM\Column(name="log", type="text", length=65535, precision=0, scale=0, nullable=true, unique=false)
     */
    private $log;

    /**
     * @var string|null
     *
     * @ORM\Column(name="type_log", type="string", length=40, precision=0, scale=0, nullable=true, unique=false)
     */
    private $typeLog;

    /**
     * @var string|null
     *
     * @ORM\Column(name="log_details", type="text", length=65535, precision=0, scale=0, nullable=true, unique=false)
     */
    private $logDetails;

    /**
     * @var \Jobs
     *
     * @ORM\ManyToOne(targetEntity="Jobs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="job_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $job;

    /**
     * @var \Users
     *
     * @ORM\ManyToOne(targetEntity="Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $user;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Executions
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set startDate.
     *
     * @param \DateTime $startDate
     *
     * @return Executions
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate.
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate.
     *
     * @param \DateTime|null $endDate
     *
     * @return Executions
     */
    public function setEndDate($endDate = null)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate.
     *
     * @return \DateTime|null
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set status.
     *
     * @param int $status
     *
     * @return Executions
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set params.
     *
     * @param string $params
     *
     * @return Executions
     */
    public function setParams($params)
    {
        $this->params = $params;

        return $this;
    }

    /**
     * Get params.
     *
     * @return string
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * Set log.
     *
     * @param string|null $log
     *
     * @return Executions
     */
    public function setLog($log = null)
    {
        $this->log = $log;

        return $this;
    }

    /**
     * Get log.
     *
     * @return string|null
     */
    public function getLog()
    {
        return $this->log;
    }

    /**
     * Set typeLog.
     *
     * @param string|null $typeLog
     *
     * @return Executions
     */
    public function setTypeLog($typeLog = null)
    {
        $this->typeLog = $typeLog;

        return $this;
    }

    /**
     * Get typeLog.
     *
     * @return string|null
     */
    public function getTypeLog()
    {
        return $this->typeLog;
    }

    /**
     * Set logDetails.
     *
     * @param string|null $logDetails
     *
     * @return Executions
     */
    public function setLogDetails($logDetails = null)
    {
        $this->logDetails = $logDetails;

        return $this;
    }

    /**
     * Get logDetails.
     *
     * @return string|null
     */
    public function getLogDetails()
    {
        return $this->logDetails;
    }

    /**
     * Set job.
     *
     * @param \Jobs $job
     *
     * @return Executions
     */
    public function setJob(\Jobs $job)
    {
        $this->job = $job;

        return $this;
    }

    /**
     * Get job.
     *
     * @return \Jobs
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * Set user.
     *
     * @param \Users $user
     *
     * @return Executions
     */
    public function setUser(\Users $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \Users
     */
    public function getUser()
    {
        return $this->user;
    }
}
