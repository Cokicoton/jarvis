<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Planifications
 *
 * @ORM\Table(name="Planifications", indexes={@ORM\Index(name="ct_planification_user_id", columns={"user_id"}), @ORM\Index(name="ct_planification_job_id", columns={"job_id"})})
 * @ORM\Entity(repositoryClass="PlanificationsRepository")
 */
class Planifications
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="smallint", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $startDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="end_date", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $endDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="frequencyTime", type="time", precision=0, scale=0, nullable=true, unique=false)
     */
    private $frequencytime;

    /**
     * @var int|null
     *
     * @ORM\Column(name="frequencyDay", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $frequencyday;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="next_term", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $nextTerm;

    /**
     * @var string
     *
     * @ORM\Column(name="params", type="text", length=65535, precision=0, scale=0, nullable=false, unique=false)
     */
    private $params;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint", precision=0, scale=0, nullable=false, unique=false)
     */
    private $status;

    /**
     * @var \Jobs
     *
     * @ORM\ManyToOne(targetEntity="Jobs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="job_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $job;

    /**
     * @var \Users
     *
     * @ORM\ManyToOne(targetEntity="Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $user;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set startDate.
     *
     * @param \DateTime $startDate
     *
     * @return Planifications
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate.
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate.
     *
     * @param \DateTime|null $endDate
     *
     * @return Planifications
     */
    public function setEndDate($endDate = null)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate.
     *
     * @return \DateTime|null
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set frequencytime.
     *
     * @param \DateTime|null $frequencytime
     *
     * @return Planifications
     */
    public function setFrequencytime($frequencytime = null)
    {
        $this->frequencytime = $frequencytime;

        return $this;
    }

    /**
     * Get frequencytime.
     *
     * @return \DateTime|null
     */
    public function getFrequencytime()
    {
        return $this->frequencytime;
    }

    /**
     * Set frequencyday.
     *
     * @param int|null $frequencyday
     *
     * @return Planifications
     */
    public function setFrequencyday($frequencyday = null)
    {
        $this->frequencyday = $frequencyday;

        return $this;
    }

    /**
     * Get frequencyday.
     *
     * @return int|null
     */
    public function getFrequencyday()
    {
        return $this->frequencyday;
    }

    /**
     * Set nextTerm.
     *
     * @param \DateTime|null $nextTerm
     *
     * @return Planifications
     */
    public function setNextTerm($nextTerm = null)
    {
        $this->nextTerm = $nextTerm;

        return $this;
    }

    /**
     * Get nextTerm.
     *
     * @return \DateTime|null
     */
    public function getNextTerm()
    {
        return $this->nextTerm;
    }

    /**
     * Set params.
     *
     * @param string $params
     *
     * @return Planifications
     */
    public function setParams($params)
    {
        $this->params = $params;

        return $this;
    }

    /**
     * Get params.
     *
     * @return string
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * Set status.
     *
     * @param int $status
     *
     * @return Planifications
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set job.
     *
     * @param \Jobs $job
     *
     * @return Planifications
     */
    public function setJob(\Jobs $job)
    {
        $this->job = $job;

        return $this;
    }

    /**
     * Get job.
     *
     * @return \Jobs
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * Set user.
     *
     * @param \Users $user
     *
     * @return Planifications
     */
    public function setUser(\Users $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \Users
     */
    public function getUser()
    {
        return $this->user;
    }
}
