<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * RoleRights
 *
 * @ORM\Table(name="Role_rights", indexes={@ORM\Index(name="ct_role_right_group_id", columns={"group_id"}), @ORM\Index(name="ct_role_right_id", columns={"role_id"}), @ORM\Index(name="ct_role_right_job_id", columns={"job_id"})})
 * @ORM\Entity(repositoryClass="RoleRightsRepository")
 */
class RoleRights
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="smallint", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=40, precision=0, scale=0, nullable=false, unique=false)
     */
    private $name;

    /**
     * @var \Groups
     *
     * @ORM\ManyToOne(targetEntity="Groups")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="group_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $group;

    /**
     * @var \Roles
     *
     * @ORM\ManyToOne(targetEntity="Roles")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="role_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $role;

    /**
     * @var \Jobs
     *
     * @ORM\ManyToOne(targetEntity="Jobs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="job_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $job;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return RoleRights
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set group.
     *
     * @param \Groups|null $group
     *
     * @return RoleRights
     */
    public function setGroup(\Groups $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group.
     *
     * @return \Groups|null
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set role.
     *
     * @param \Roles $role
     *
     * @return RoleRights
     */
    public function setRole(\Roles $role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role.
     *
     * @return \Roles
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set job.
     *
     * @param \Jobs|null $job
     *
     * @return RoleRights
     */
    public function setJob(\Jobs $job = null)
    {
        $this->job = $job;

        return $this;
    }

    /**
     * Get job.
     *
     * @return \Jobs|null
     */
    public function getJob()
    {
        return $this->job;
    }
}
