<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * ParentJobs
 *
 * @ORM\Table(name="Parent_jobs", indexes={@ORM\Index(name="ct_child_id", columns={"child_id"}), @ORM\Index(name="ct_parent_id", columns={"parent_id"})})
 * @ORM\Entity(repositoryClass="ParentJobsRepository")
 */
class ParentJobs
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="smallint", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="child_order", type="smallint", precision=0, scale=0, nullable=true, unique=false)
     */
    private $childOrder;

    /**
     * @var \Jobs
     *
     * @ORM\ManyToOne(targetEntity="Jobs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="child_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $child;

    /**
     * @var \Jobs
     *
     * @ORM\ManyToOne(targetEntity="Jobs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $parent;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set childOrder.
     *
     * @param int|null $childOrder
     *
     * @return ParentJobs
     */
    public function setChildOrder($childOrder = null)
    {
        $this->childOrder = $childOrder;

        return $this;
    }

    /**
     * Get childOrder.
     *
     * @return int|null
     */
    public function getChildOrder()
    {
        return $this->childOrder;
    }

    /**
     * Set child.
     *
     * @param \Jobs $child
     *
     * @return ParentJobs
     */
    public function setChild(\Jobs $child)
    {
        $this->child = $child;

        return $this;
    }

    /**
     * Get child.
     *
     * @return \Jobs
     */
    public function getChild()
    {
        return $this->child;
    }

    /**
     * Set parent.
     *
     * @param \Jobs $parent
     *
     * @return ParentJobs
     */
    public function setParent(\Jobs $parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent.
     *
     * @return \Jobs
     */
    public function getParent()
    {
        return $this->parent;
    }
}
