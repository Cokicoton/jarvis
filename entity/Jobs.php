<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Jobs
 *
 * @ORM\Table(name="Jobs", indexes={@ORM\Index(name="ct_group_id", columns={"group_id"})})
 * @ORM\Entity(repositoryClass="JobsRepository")
 */
class Jobs
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="smallint", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=40, precision=0, scale=0, nullable=false, unique=false)
     */
    private $name;

    /**
     * @var int|null
     *
     * @ORM\Column(name="group_order", type="smallint", precision=0, scale=0, nullable=true, unique=false)
     */
    private $groupOrder;

    /**
     * @var string|null
     *
     * @ORM\Column(name="script", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $script;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=65535, precision=0, scale=0, nullable=true, unique=false)
     */
    private $description;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="last_exec", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $lastExec;

    /**
     * @var \Groups
     *
     * @ORM\ManyToOne(targetEntity="Groups")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="group_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $group;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Jobs
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set groupOrder.
     *
     * @param int|null $groupOrder
     *
     * @return Jobs
     */
    public function setGroupOrder($groupOrder = null)
    {
        $this->groupOrder = $groupOrder;

        return $this;
    }

    /**
     * Get groupOrder.
     *
     * @return int|null
     */
    public function getGroupOrder()
    {
        return $this->groupOrder;
    }

    /**
     * Set script.
     *
     * @param string|null $script
     *
     * @return Jobs
     */
    public function setScript($script = null)
    {
        $this->script = $script;

        return $this;
    }

    /**
     * Get script.
     *
     * @return string|null
     */
    public function getScript()
    {
        return $this->script;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return Jobs
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set lastExec.
     *
     * @param \DateTime|null $lastExec
     *
     * @return Jobs
     */
    public function setLastExec($lastExec = null)
    {
        $this->lastExec = $lastExec;

        return $this;
    }

    /**
     * Get lastExec.
     *
     * @return \DateTime|null
     */
    public function getLastExec()
    {
        return $this->lastExec;
    }

    /**
     * Set group.
     *
     * @param \Groups|null $group
     *
     * @return Jobs
     */
    public function setGroup(\Groups $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group.
     *
     * @return \Groups|null
     */
    public function getGroup()
    {
        return $this->group;
    }
}
