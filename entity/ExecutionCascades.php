<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * ExecutionCascades
 *
 * @ORM\Table(name="Execution_cascades", indexes={@ORM\Index(name="ct_execution_multi_job_id", columns={"job_id"}), @ORM\Index(name="ct_execution_multi_execution_parent_id", columns={"execution_parent_id"}), @ORM\Index(name="ct_execution_multi_execution_id", columns={"execution_id"})})
 * @ORM\Entity(repositoryClass="ExecutionCascadesRepository")
 */
class ExecutionCascades
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="smallint", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="execution_order", type="smallint", precision=0, scale=0, nullable=true, unique=false)
     */
    private $executionOrder;

    /**
     * @var \Executions
     *
     * @ORM\ManyToOne(targetEntity="Executions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="execution_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $execution;

    /**
     * @var \Executions
     *
     * @ORM\ManyToOne(targetEntity="Executions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="execution_parent_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $executionParent;

    /**
     * @var \Jobs
     *
     * @ORM\ManyToOne(targetEntity="Jobs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="job_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $job;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set executionOrder.
     *
     * @param int|null $executionOrder
     *
     * @return ExecutionCascades
     */
    public function setExecutionOrder($executionOrder = null)
    {
        $this->executionOrder = $executionOrder;

        return $this;
    }

    /**
     * Get executionOrder.
     *
     * @return int|null
     */
    public function getExecutionOrder()
    {
        return $this->executionOrder;
    }

    /**
     * Set execution.
     *
     * @param \Executions|null $execution
     *
     * @return ExecutionCascades
     */
    public function setExecution(\Executions $execution = null)
    {
        $this->execution = $execution;

        return $this;
    }

    /**
     * Get execution.
     *
     * @return \Executions|null
     */
    public function getExecution()
    {
        return $this->execution;
    }

    /**
     * Set executionParent.
     *
     * @param \Executions $executionParent
     *
     * @return ExecutionCascades
     */
    public function setExecutionParent(\Executions $executionParent)
    {
        $this->executionParent = $executionParent;

        return $this;
    }

    /**
     * Get executionParent.
     *
     * @return \Executions
     */
    public function getExecutionParent()
    {
        return $this->executionParent;
    }

    /**
     * Set job.
     *
     * @param \Jobs $job
     *
     * @return ExecutionCascades
     */
    public function setJob(\Jobs $job)
    {
        $this->job = $job;

        return $this;
    }

    /**
     * Get job.
     *
     * @return \Jobs
     */
    public function getJob()
    {
        return $this->job;
    }
}
