<?php 
  include '../config/config.php';
  include '../lang/' . $lang . '.php';
  $current = '';
  include '../element/header.php';
  $job_id = intval($_GET['id']);

  if(isset($_GET['PlanificationStartDate']) && $_GET['PlanificationStartDate'] != ''){
    $job = $entityManager->getRepository('Jobs')->findOneBy( array('id' => $job_id));
    $user = $entityManager->getRepository('Users')->findOneBy( array('id' => 1));

    $planification = new Planifications();
    $planification->setJob($job);
    $planification->setUser($user);
    $startDate = DateTime::createFromFormat('Y-m-d\TH:i', $_GET['PlanificationStartDate']);
    $planification->setStartDate($startDate);
    if($_GET['PlanificationFrequency'] != ''){
      $temp = explode(':', $_GET['PlanificationFrequency']);
      $h = strval(intval($temp[0]) % 24);
      $m = ($temp[1]);
      $d = floor(intval($temp[0]) / 24);
      $planification->setFrequencyTime(DateTime::createFromFormat('H:i:s', $h . ':' . $m . ':' . '00'));
      $planification->setFrequencyDay($d);
    }
    $planification->setNextTerm($startDate);

    if($_GET['PlanificationEndDate'] != ''){
      $planification->setEndDate(DateTime::createFromFormat('Y-m-d\TH:i', $_GET['PlanificationEndDate']));
    }
    $paramsExec = '';
    $paramsDb = '{';
    foreach($_GET as $key => $value){
      if(!in_array($key, ['id', 'PlanificationStartDate', 'PlanificationFrequency', 'PlanificationEndDate'])){
        $paramsExec = $paramsExec . $value . ' ';
        $paramsDb = $paramsDb . '"' . $key . '"' . ':' . '"' . $value . '"' . ',';      
      }
    }

    if($paramsDb == '{'){//??
      $paramsDb = $paramsDb . '}';
    } else {
      $paramsDb = $paramsDb . '}';
    }
    if(substr($paramsDb, -2) == ',}'){
      $paramsDb = substr($paramsDb, 0, -2) . '}';
    }
    $planification->setParams($paramsDb);
    $planification->setStatus(1);
    $entityManager->persist($planification);
    $entityManager->flush();
    header('Location: job.php?id=' . $job_id);
    exit();
    
  }
  if(isset($_GET['multi']) && $_GET['multi'] == "true") { //Only one job to run
    //Créer une execution pour le parent avec tous les params dans params
    // les paramètres arrivent dans le $_GET sous la forme [ID_JOB]_NOM_PARAM = VALUE_PARAM
    //Lancer le fils 1 avec les params
    $job = $entityManager->getRepository('Jobs')->findOneBy( array('id' => $job_id));
    $user = $entityManager->getRepository('Users')->findOneBy( array('id' => 1));

    $params = [];
    foreach($_GET as $key => $value){
      if($key != 'id' && $key != 'multi'){
        $id = intval(explode('_',$key)[0]);
        $params[$id][explode(explode('_',$key)[0] . '_',$key)[1]] = $value;
      }  
    }

  
    $execution = new Executions();
    $execution->setJob($job);
    $execution->setUser($user);
    $execution->setStartDate(new DateTime());
    $job->setLastExec($execution->getStartDate());
    $execution->setName('exec_' . $job->getId() . '_' . $user->getId() . '_' . $execution->getStartDate()->format('Y-m-d_H-i-s'));
    $execution->setParams(json_encode($params));
    $execution->setStatus(-1);
    $entityManager->persist($execution);
    $entityManager->persist($job);


    $first = $entityManager->getRepository('ParentJobs')->findOneBy( array('parent' => $job_id), array('childOrder' => 'ASC'));
    $first->getChild()->getId();   

    $paramsExec = '';
    $paramsDb = '{';
    foreach($params[$first->getChild()->getId()] as $key => $value){
      if($key != 'id'){
        $paramsExec = $paramsExec . $value . ' ';
        $paramsDb = $paramsDb . '"' . $key . '"' . ':' . '"' . $value . '"' . ',';      
      }
    }
    if($paramsExec == ''){
      $paramsExec = substr($paramsExec,0, -1);
      $paramsDb = substr($paramsDb,0, -1) . '}';
    } else {
      $paramsDb = $paramsDb . '}';
    }

    $result = exec($first->getChild()->getScript() . ' ' . $paramsExec);
    $log = explode(' ', $result)[1];
    $typeLog = explode(' ', $result)[0];

    $executionFirst = new Executions();
    $executionFirst->setJob($first->getChild());
    $executionFirst->setUser($user);
    $executionFirst->setStartDate(new DateTime());
    $first->getChild()->setLastExec($executionFirst->getStartDate());
    $executionFirst->setName('exec_' . $first->getChild()->getId() . '_' . $user->getId() . '_' . $executionFirst->getStartDate()->format('Y-m-d_H-i-s'));
    $executionFirst->setParams(json_encode($params[$first->getChild()->getId()]));
    $executionFirst->setLog($log);
    $executionFirst->setTypeLog($typeLog);
    $executionFirst->setStatus(-1);
    $entityManager->persist($executionFirst);
    $entityManager->persist($first->getChild());

    $children = $entityManager->getRepository('ParentJobs')->findBy( array('parent' => $job_id), array('childOrder' => 'ASC'));
    foreach($children as $child){
      $executionCascade = new ExecutionCascades();
      $executionCascade->setExecutionParent($execution);
      $executionCascade->setJob($child->getChild());
      if($child->getChild()->getId() == $first->getChild()->getId()){
        $executionCascade->setExecution($executionFirst);
      }
      $executionCascade->setExecutionOrder($child->getChildOrder());
      $entityManager->persist($executionCascade);
    }

    $entityManager->flush();
    header('Location: job.php?id=' . $job_id);
    exit();
  } else { //Only one job to run    
    $paramsExec = '';
    $paramsDb = '{';
    foreach($_GET as $key => $value){
      if($key != 'id'){
        $paramsExec = $paramsExec . $value . ' ';
        $paramsDb = $paramsDb . '"' . $key . '"' . ':' . '"' . $value . '"' . ',';      
      }
    }
    if($paramsExec == ''){
      $paramsExec = '';
      $paramsDb = '{}';
    } else {
      if(substr($paramsDb, -1) == ','){
        $paramsDb = substr($paramsDb, 0, -1);
      }
      $paramsDb = $paramsDb . '}';
    }
  
    $job = $entityManager->getRepository('Jobs')->findOneBy( array('id' => $job_id));
    $user = $entityManager->getRepository('Users')->findOneBy( array('id' => 1));
    $result = exec($job->getScript() . ' ' . $paramsExec);
    $log = explode(' ', $result)[1];
    $typeLog = explode(' ', $result)[0];
    
    $execution = new Executions();
    $execution->setJob($job);
    $execution->setUser($user);
    $execution->setStartDate(new DateTime());
    $job->setLastExec($execution->getStartDate());
    $execution->setName('exec_' . $job->getId() . '_' . $user->getId() . '_' . $execution->getStartDate()->format('Y-m-d_H-i-s'));
    $execution->setParams($paramsDb);
    $execution->setLog($log);
    $execution->setTypeLog($typeLog);
    $execution->setStatus(-1);
    $entityManager->persist($execution);
    $entityManager->persist($job);
    $entityManager->flush();
    header('Location: job.php?id=' . $job_id);
    exit();
  }
?>
