<?php 
  include '../config/config.php';
  include '../lang/' . $lang . '.php';
  $current = 'new_group';
  include '../element/header.php';
  if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $group = new Groups();
    $group->setName($_POST['addGroupName']);
    $group->setDescription($_POST['addGroupDescription']);
    $group->setOrder(intval($_POST['addGroupOrder']));
    if($_POST['color'] != ""){
      $group->setColor($_POST['color']);
    }    

    $entityManager->persist($group);

    if($_POST['idJobs'] != ""){
      $jobs = explode(',', $_POST['idJobs']);
      foreach($jobs as $jobString){
        $idJob = intval($jobString);
        $job= $entityManager->getRepository('Jobs')->findOneBy(array('id' => $idJob));
        $job->setGroup($group);
        $entityManager->persist($job);
      }
    }
	
    $entityManager->flush();
  }

?>
   <form method="POST">
    <div class="container mt-5">
      <h2><?php echo $new_group_page_title; ?></h2>
      <div class="form-group w-50">
        <label for="addGroupName"><?php echo $new_group_page_label_name_field; ?></label>
        <input type="text" class="form-control required" id="addGroupName" name="addGroupName" placeholder="<?php echo $new_group_page_placeholder_name_field; ?>">
      </div>
      <div class="form-group w-50">
        <label for="addGroupDescription"><?php echo $new_group_page_label_description_field; ?></label>
        <textarea class="form-control required" id="addGroupDescription" name="addGroupDescription" rows="3"></textarea>
      </div>
      <div class="form-group">
          <label for="addGroupJobsTemp" class="d-block w-25"><?php echo $new_group_page_label_Jobs; ?></label>
        <div class="w-25 d-inline">
          <select multiple="" class="form-control w-25 d-inline" id="addGroupJobs">
          <?php
            $jobs= $entityManager->getRepository('Jobs')->findBy(array(), array('id' => 'ASC'));
            $cptJob = 0;
            foreach($jobs as $job){
              $selected = '';
              if($cptJob == 0){
                $selected = 'selected';
              }
              echo '<option class="" data-type="job" data-id="' . $job->getId() . '" value="' . $job->getId() . '" ' . $selected . '>' . $job->getName() . '</option>';
              $cptJob++;
            }
              
          ?>
          </select>
        </div>
        <div class="w-25 d-inline">
          <button type="button" id="addJob"class="btn btn-primary mb-5 ml-5 mr-5"><?php echo $new_group_page_add_job_button; ?></button>
        </div>
        <div class="w-25 d-inline">
          <select multiple="" class="form-control w-25 d-inline" id="addedGroupJobs">
            <option class="" data-type="job" data-id="0" value="null"><?php echo $new_group_page_no_job_add; ?></option>
          </select>
        </div>
      </div>
      <div class="form-group">
          <label for="addGroupOrder" class="d-block w-25"><?php echo $new_group_page_label_order_field; ?></label>
        <div class="w-25">
          <input type="number" class="form-control required" id="addGroupOrder" name="addGroupOrder" value="1">
        </div>
      </div>
      <div class="form-group mb-4 w-50" id="colorSelect">
        <label for="adGroupName" class="d-block"><?php echo $new_group_page_label_color_field; ?></label>
        <?php
          $colors= $entityManager->getRepository('Colors')->findBy(array(), array('id' => 'ASC'));
          $cpt = 0;
          foreach($colors as $color){
            if($cpt % 3 == 0) {
              echo '<br>';
            }
            echo '<button type="button" class="btn w-25 ' . str_replace('bg-', 'btn-', $color->getClass()) . '" data-value="' . $color->getClass() . '">' . $color->getClass() . '</button>';
            $cpt++;
          }
        ?>

      </div>
      <input type="hidden" id="idJobs" name="idJobs" value="">
      <input type="hidden" id="color" name="color" value="">  
      <button type="submit" id="submitAddGroup" class="btn btn-primary d-block" disabled><?php echo $new_group_page_submit_button; ?></button>
    </div>
   <form>
    <script src="../js/new_group.js"></script>
<?php include '../element/footer.php'; ?>
