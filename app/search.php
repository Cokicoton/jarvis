<?php 
  include '../config/config.php';
  include '../lang/' . $lang . '.php';
  $current = 'search';
?>
<?php include '../element/header.php'; ?>
<?php
        $search = $_GET['search'];
        $queryJobs = $entityManager->getRepository('Jobs')->createQueryBuilder('j')
               ->where('j.name LIKE :searchLike')
               ->orWhere('j.description LIKE :searchLike')
               ->setParameter('searchLike', '%' . $search . '%');
        $queryGroups = $entityManager->getRepository('Groups')->createQueryBuilder('g')
               ->where('g.name LIKE :searchLike')
               ->orWhere('g.description LIKE :searchLike')
               ->setParameter('searchLike', '%' . $search . '%');
        $jobs = $queryJobs->getQuery()->getResult();
        $groups = $queryGroups->getQuery()->getResult();
?>
    <div class="container mt-5">
      <h2 class="mb-4"><?php echo $search_page_title; ?></h2>
      <table class="table">
        <tbody>
          <tr class="table-primary">
            <td class="text-center text-white"><h5><?php echo $jobs_page_title?></h5></td>
          </tr>
          <tr class="">
            <td class="">
              <?php
                if(count($jobs) == 0){ // No job found
                  echo $search_page_no_job_found;
                } else { // At least one job found
                  echo '              <div class="list-group">' . "\n";
                  foreach($jobs as $job){
                    echo '                <a href="#" class="list-group-item list-group-item-action flex-column align-items-start clickable cursor-pointer" data-type="job" data-id="' . $job->getId() . '">
                  <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1">' . $job->getName() . '</h5>
                  </div>
                  <p class="mb-1">' . $job->getDescription() . '</p>
                </a>' . "\n";
                  }
                }
                echo '              </div>';
              ?>
            </td>
          </tr>
        </tblody>
      </table>
      <table class="table">
        <tbody>
          <tr class="table-primary">
            <td class="text-center text-white"><h5><?php echo $groups_page_title?></h5></td>
          </tr>
          <tr class="">
            <td class="">
              <?php
                if(count($groups) == 0){ // No group found
                  echo $search_page_no_group_found;
                } else { // At least one group found
                  echo '              <div class="list-group">' . "\n";
                  foreach($groups as $group){
                    echo '                <a href="#" class="list-group-item list-group-item-action flex-column align-items-start clickable cursor-pointer" data-type="group" data-id="' . $group->getId() . '">
                  <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1">' . $group->getName() . '</h5>
                  </div>
                  <p class="mb-1">' . $group->getDescription() . '</p>
                </a>' . "\n";
                  }
                }
                echo '              </div>';
              ?>
            </td>
          </tr>
        </tblody>
      </table>
    </div>
<?php include '../element/footer.php'; ?>
