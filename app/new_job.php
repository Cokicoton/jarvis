<?php 
  include '../config/config.php';
  include '../lang/' . $lang . '.php';
  $current = 'new_job';
  include '../element/header.php';
  if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    $job = new Jobs();
    $job->setName($_POST['addJobName']);
    $job->setDescription($_POST['addJobDescription']);
    if($_POST['addJobGroup'] != ''){
      $group= $entityManager->getRepository('Groups')->findOneBy(array('id' => intval($_POST['addJobGroup'])));
      $job->setGroup($group);
    }
    if($_POST['addJobGroupOrder'] != ''){
      $job->setGroupOrder(intval($_POST['addJobGroupOrder']));
    }    
    $job->setScript($_POST['addScriptName']);

    $entityManager->persist($job);

    $listParams = [];
    $listDescriptions = [];
    $listDefaults = [];
    $listParamsEntity = [];
    if($_POST['params'] != ''){
      $params = explode(';&', $_POST['params']);
      array_splice($params, 0, 1);
      foreach($params as $param){
        $temp = explode('&_&', substr($param, 2));
        $tempBis = explode('<<>>', $temp[0]);
        $paramName = $tempBis[0];
        $paramDescription = $tempBis[1];
        $paramDefault =  $tempBis[2];
        $temp2 = explode('&_&', $temp[1]);
        $paramTypeId = intval($temp2[0]);
        $listParams[$paramName] = $paramTypeId;
        $listDescriptions[$paramName] = $paramDescription;
        $listDefaults[$paramName] = $paramDefault;
      }

      foreach($listParams as $parameter => $typeId){
        $parameters = new Parameters();
        $parameters->setName($parameter);
        $type = $entityManager->getRepository('TypeLists')->findOneBy(array('id' => intval($typeId)));
        $parameters->setType($type);
        if(isset($listDescriptions[$parameter]) && $listDescriptions[$parameter] != ''){
          $parameters->setDescription($listDescriptions[$parameter]);
        }
        $listParamsEntity[$parameter] = $parameters;
        $entityManager->persist($parameters);

        $jobParameters = new JobParams();
        $jobParameters->setName($parameters->getName());
        $jobParameters->setDescription($parameters->getDescription());
        $jobParameters->setJob($job);
        $jobParameters->setParam($parameters);
        if(isset($listDefaults[$parameter]) && $listDefaults[$parameter] != ''){
          $jobParameters->setDefaultValue($listDefaults[$parameter]);
        }
        $entityManager->persist($jobParameters);
      }
    }

    $selectValues = [];
    if($_POST['selectValues'] != ''){
      $selects = explode('&[&', $_POST['selectValues']);
      array_splice($selects, 0, 1);
      foreach($selects as $select){
        $temp = substr($select, 0, strlen($select)-3);
        $temp2 = explode('&_&', $temp);
        $paramName = $temp2[0];
        $listValues = $temp2[1];
        $temp3 = explode('&,&', $listValues);
        $tempList = [];
        foreach($temp3 as $tempValue){
          array_push($tempList, $tempValue);
        }
        $selectValues[$paramName] = $tempList;
      }

      foreach($selectValues as $paramName => $valuesList){
        $paramEntity = $listParamsEntity[$paramName];
        foreach($valuesList as $value){
          $valueList = new ValueLists();
          $valueList->setParam($paramEntity);
          $temp = explode('[desc=', $value);
          $valueList->setValue($temp[0]);
          if(count($temp) > 1){
            $valueList->setDescription(substr($temp[1], 0, -1));
          }
          $entityManager->persist($valueList);
        }        
      }
    }

    $entityManager->flush();
  }

?>
   <form method="POST">
    <div class="container mt-5">
      <h2><?php echo $new_job_page_title; ?></h2>
      <div class="form-group w-50">
        <label for="addJobName"><?php echo $new_job_page_label_name_field; ?></label>
        <input type="text" class="form-control required" id="addJobName" name="addJobName" placeholder="<?php echo $new_job_page_placeholder_name_field; ?>">
      </div>
      <div class="form-group w-50">
        <label for="addJobDescription"><?php echo $new_job_page_label_description_field; ?></label>
        <textarea class="form-control required" id="addJobDescription" name="addJobDescription" rows="3"></textarea>
      </div>
      <div class="form-group  w-50">
          <label for="addJobGroup" class="d-block"><?php echo $new_job_page_label_Group; ?></label>
        <div class="">
          <select multiple="" class="form-control" id="addJobGroup" name="addJobGroup">
            <option class="" data-type="group" data-id="null" value="" selected><?php echo $new_job_page_no_group; ?></option>
          <?php
            $groups= $entityManager->getRepository('Groups')->findBy(array(), array('id' => 'ASC'));
            foreach($groups as $group){
              echo '<option class="" data-type="group" data-id="' . $group->getId() . '" value="' . $group->getId() . '" >' . $group->getName() . '</option>';
            }
              
          ?>
          </select>
      </div>
      <div class="form-group mt-3">
        <label for="addJobGroupOrder" class="d-block w-25"><?php echo $new_job_page_label_group_order_field; ?></label>
        <div class="w-50">
          <input type="number" class="form-control" id="addJobGroupOrder" name="addJobGroupOrder" value="1">
        </div>
      </div>
      <div class="form-group">
        <label for="addScriptName"><?php echo $new_job_page_label_script_field; ?></label>
        <input type="text" class="form-control required" id="addScriptName" name="addScriptName" placeholder="<?php echo $new_job_page_placeholder_script_field; ?>">
      </div>
      <div class="form-group">
          <label for="addParamType" class="d-block"><?php echo $new_job_page_label_parameters; ?></label>
        <div class="w-25 d-inline">
          <select multiple="" class="form-control d-inline" id="addParamType">
          <?php
            $types= $entityManager->getRepository('TypeLists')->findBy(array('enabled' => true), array('order' => 'ASC'));
            $cptType = 0;
            foreach($types as $type){
              $selected = '';
              if($cptType == 0){
                $selected = 'selected';
              }
              echo '<option class="" data-type="job" data-id="' . $type->getId() . '" value="' . $type->getId() . '" ' . $selected . '>' . $type->getName() . '</option>';
              $cptType++;
            }
              
          ?>
          </select>
        </div>
        <div class="w-25 d-inline">
          <input type="text" class="form-control" id="addParamName" name="addParamName" placeholder="<?php echo $new_job_page_placeholder_param_name_field; ?>">
        </div>
      <div class="w-25 d-inline">
        <textarea class="form-control" id="addParamDescription" name="addParamDescription" rows="3" placeholder="<?php echo $new_job_page_placeholder_param_description_field; ?>"></textarea>
      </div>
        <div class="w-25 d-inline">
          <input type="text" class="form-control" id="addParamDefault" name="addParamDefault" placeholder="<?php echo $new_job_page_placeholder_param_default_field; ?>">
        </div>
        <div class="w-25 d-inline">
          <button type="button" id="addParameter"class="btn btn-primary ml-5 mr-5 mt-3"><?php echo $new_job_page_add_parameter_button; ?></button>
        </div>
        <div class="d-block mt-3 mb-5 " id="paramsList">          
        </div>
      </div>
      <input type="hidden" id="params" name="params" value="">
      <input type="hidden" id="selectValues" name="selectValues" value="">
      <button type="submit" id="submitAddJob" class="btn btn-primary d-block" disabled><?php echo $new_job_page_submit_button; ?></button>
    </div>
    <div id="selectModal" class="modal" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title"><?php echo $new_job_page_modal_select_title; ?></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p><?php echo $new_job_page_modal_select_description; ?></p>
            <textarea class="form-control" id="addJobModalSelectList" name="addJobModalSelectList" rows="10"></textarea>
          </div>
          <div class="modal-footer">
            <button id="saveSelectModal" type="button" class="btn btn-primary"><?php echo $new_job_page_modal_select_button; ?></button>
          </div>
        </div>
      </div>
    </div>
   <form>
    <script src="../js/new_job.js"></script>
<?php include '../element/footer.php'; ?>
