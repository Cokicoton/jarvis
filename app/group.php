<?php 
  include '../config/config.php';
  include '../lang/' . $lang . '.php';
  $current = 'group';
?>
<?php include '../element/header.php'; ?>
<?php
        $group_id = intval($_GET['id']);
        $group = $entityManager->getRepository('Groups')->findOneBy( array('id' => $group_id));
        $jobs = $entityManager->getRepository('Jobs')->findBy( array('group' => $group), array('groupOrder' => 'ASC'));
?>
    <div class="container mt-5">
      <h2><?php echo $group->getName(); ?></h2>
      <p><?php echo $group->getDescription(); ?></p>
      <?php
        if(count($jobs) == 0) { // No job for this group
          echo $group_page_no_jobs;
        } else { // At leat one job for this group
          foreach($jobs as $job){
            echo '          <div class="alert alert-dismissible ' . (is_null($group->getColor())? $default_color_groups : $group->getColor()) . ' clickable cursor-pointer hover-dark" data-type="job" data-id="' . $job->getId() . '">
            <h4 class="alert-heading">' . $job->getName() . '</h4>
            <p class="mb-0">' . $job->getDescription() . '</a>.</p>
          </div>';
          }
        }
      ?>
    </div>
<?php include '../element/footer.php'; ?>
