<?php 
  include '../config/config.php';
  include '../lang/' . $lang . '.php';
  $current = !isset($current) ? 'executions' : $current;
?>
<?php include '../element/header.php'; ?>
<?php
  $executions = $entityManager->getRepository('Executions')->findBy(array(), array('startDate' => 'DESC'));
?>
    <div class="container mt-5">
      <h2><?php echo $executions_page_title; ?></h2>
          <?php
            if(count($executions) == 0) {// no execution in Executions table
              echo $executions_page_empty;
            } else {// at leat one execution in Executions table
              echo '          <table class="table table-hover">
            <thead>
              <tr style="color:#eee">
                <th scope="col">' . $executions_page_column_job . '</th>
                <th scope="col">' . $executions_page_column_start . '</th>
                <th scope="col">' . $executions_page_column_end . '</th>
                <th scope="col">' . $executions_page_column_duration . '</th>
                <th scope="col">' . $executions_page_column_status . '</th>
              </tr>
            </thead>
            <tbody>' . "\n";
              $cpt = 0;
              foreach($executions as $execution){
                echo '              <tr class="' . ($cpt % 2 == 0 ? 'table-light' : 'table-dark') . ' clickable cursor-pointer" data-type="execution" data-id="' . $execution->getId() . '">
                <td scope="row" class="font-weight-bold">' . $execution->getJob()->getName() . '</th>
                <td scope="row">' . $execution->getStartDate()->format('d/m/Y H:i:s') . '</th>
                <td>' . (is_null($execution->getEndDate())?$job_page_running_exec:$execution->getEndDate()->format('d/m/Y H:i:s')) . ($execution->getStatus() == -1 ? ' <img src="../img/progress.gif" class="h1-5em float-right">':'') .  '</td>
                <td>' . (!is_null($execution->getEndDate())? $execution->getStartDate()->diff($execution->getEndDate())->format('%H h %I m %S s'):'') . '</td>
                <td>' . $status_traduction[$execution->getStatus()] . '</td>
              </tr>' . "\n";
                $cpt++;
              }
              echo '              </tbody>
            </table>' . "\n";
            }
          ?> 
    </div>
<?php include '../element/footer.php'; ?>

