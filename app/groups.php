<?php 
  include '../config/config.php';
  include '../lang/' . $lang . '.php';
  $current = !isset($current) ? 'groups' : $current;
?>
<?php include '../element/header.php'; ?>
    <div class="float-right display-3 text-white bg-primary mr-5 mt-3 text-center rounded-circle cursor-pointer add-button hover-dark" data-page="groups">+</div>
    <div class="container mt-5">
      <h2 class="text-center"><?php echo $groups_page_title; ?></h2>
      <?php
        $groups= $entityManager->getRepository('Groups')->findAll( array('order' => 'ASC'));        
        if (count($groups) == 0){ // no group in Groups table
          echo '    <div class="">
                      ' . $groups_page_empty . '
                    </div>' . "\n";
        } else { // at leat one group in Groups table
          echo '    <div class="d-flex flex-wrap justify-content-around">' . "\n";
          foreach($groups as $group){

          echo '        <div class="card text-white ' . (is_null($group->getColor())? $default_color_groups : $group->getColor()) . ' mb-3 cursor-pointer hover-dark clickable h280px" style="max-width: 20rem; width: 30%" data-type="group" data-id="' . $group->getId() . '">
          <div class="card-header">' . $group->getId() . '</div>
          <div class="card-body">
            <h4 class="card-title">' . $group->getName() . '</h4>
            <p class="card-text h150px overflow-hidden">' . $group->getDescription() . '</p>
          </div>
        </div>';
          }
        }      
      ?>
      </div>
    </div>  
<?php include '../element/footer.php'; ?>
