<?php 
  include '../config/config.php';
  include '../lang/' . $lang . '.php';
  $current = 'execution';
?>
<?php include '../element/header.php'; ?>
<?php
        $execution_id = intval($_GET['id']);
        $execution = $entityManager->getRepository('Executions')->findOneBy( array('id' => $execution_id));
?>
    <div class="container mt-5">
      <h2><?php echo $execution->getJob()->getName(); echo (is_null($execution->getEndDate()) && $execution->getStatus() == -1 ? ' <img src="../img/progress.gif" class="h1-5em float-right ">':'');?></h2>
      <h5><?php echo $execution->getStartDate()->format('d/m/Y H:i:s'); ?></h5>
      <ul class="nav nav-tabs mt-3">
        <li class="nav-item">
          <a class="nav-link active" data-toggle="tab" href="#infos"><?php echo $execution_page_infos_tab; ?></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="#parameters"><?php echo $execution_page_parameters_tab; ?></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="#logs"><?php echo $execution_page_logs_tab; ?></a>
        </li>
      </ul>
      <div id="myTabContent" class="tab-content mt-3 w-75">
        <div class="tab-pane fade show active" id="infos">
          <ul class="list-group">
            <li class="list-group-item d-flex justify-content-between align-items-center">
              <?php echo $executions_page_column_job ?>
              <span class="bg-primary w-50 text-center p-1 text-white clickable hover-dark cursor-pointer" data-type="job" data-id="<?php echo $execution->getJob()->getId() ?>"><?php echo $execution->getJob()->getName(); ?></span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
              <?php echo $execution_page_run_by ?>
              <span class="bg-primary w-50 text-center p-1 text-white"><?php echo $execution->getUser()->getName(); ?></span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
              <?php echo $executions_page_column_start ?>
              <span class="bg-primary w-50 text-center p-1 text-white"><?php echo $execution->getStartDate()->format('d/m/Y H:i:s'); ?></span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
              <?php echo $executions_page_column_end ?>
              <span class="bg-primary w-50 text-center p-1 text-white"><?php echo is_null($execution->getEndDate()) ? 'En cours' : $execution->getEndDate()->format('d/m/Y H:i:s'); ?></span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
              <?php echo $executions_page_column_duration ?>
              <?php echo (!is_null($execution->getEndDate())? '<span class="bg-primary w-50 text-center p-1 text-white">' . $execution->getStartDate()->diff($execution->getEndDate())->format('%H h %I m %S s') . '</span>':''); ?>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
              <?php echo $executions_page_column_status ?>
              <span class="bg-primary w-50 text-center p-1 text-white"><?php echo $status_traduction[$execution->getStatus()]; ?></span>
            </li>
          </ul>
        </div>
        <div class="tab-pane fade show" id="parameters"><?php/************************-***************/?>
          <?php
            if($execution->getParams() == '{}'){// no parameter for this job
              echo $execution_page_no_parameter;
            } else {// at leat one parameter for this job
              echo '          <ul class="list-group">' . "\n";
              $params = json_decode($execution->getParams());
              foreach($params as $key => $value){
                if(!in_array($key, ['PlanificationStartDate', 'PlanificationFrequency', 'PlanificationEndDate'])){
                  echo '                <li class="list-group-item d-flex justify-content-between align-items-center">
              ' . $key . '
              <span class="bg-primary w-50 text-center p-1 text-white">' . $value . '</span>
            </li>';
                }                
              }
              echo '          </ul>' . "\n";
            }
          ?>
        </div>
        <div class="tab-pane fade show" id="logs">
          <div class="jumbotron">            
            <?php
              if(is_null($execution->getLogDetails())){
                echo 'Aucune log';
              } else {
                echo '            <h5>' . (is_null($execution->getLog())? $execution_page_logs_tab : $execution->getLog()) . '</h5>' . "\n";
                echo '            <p class="border mt-3 p-2">'  . str_replace("\n", "<br>", $execution->getLogDetails()) .  '</p>' . "\n";
              }
            ?>
          </div>
        </div>
      </div>
    </div>
<?php include '../element/footer.php'; ?>
