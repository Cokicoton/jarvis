<?php 
  include '../config/config.php';
  include '../config/version.php';
  include '../lang/' . $lang . '.php';
  $current = 'about';
?>
<?php include '../element/header.php'; ?>
    <div class="container mt-5">
      <h2><?php echo $about_page_title; ?></h2>
      <div class="card border-primary mb-3 p-2 h6">
      <div class="h5">Jarvis</div>
      <div><?php echo $about_page_version ?> : &nbsp;&nbsp;&nbsp;&nbsp;<?php echo $version ?></div>
      <div><?php echo $about_page_license ?> : &nbsp;&nbsp;&nbsp;&nbsp;<a href="https://www.gnu.org/licenses/gpl-3.0.txt">GNU General Public License v3.0</a></div>
      <div><?php echo $about_page_git_repository ?> : &nbsp;<a href="https://framagit.org/Cokicoton/jarvis">Framagit</a></div>
      </div>
      <div class="alert alert-dismissible alert-warning">
        <h4 class="alert-heading">A faire</h4>
        <p class="mb-0"> 
          <ul>
            <li>Gestion des utilisateurs</li>
            <li>Ajouter possibilté de recevoir mail lorsque job finit (ajouter mail dans user)</li>
            <li>Informations redondantes (inutiles) dans la table job_params -> name, description car l'appli prend ceux de la table params</li>
          </ul>
        </p>
      </div>
    </div>
<?php include '../element/footer.php'; ?>

