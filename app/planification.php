<?php 
  include '../config/config.php';
  include '../lang/' . $lang . '.php';
  $current = 'planification';
?>
<?php include '../element/header.php'; ?>
<?php
        $planification_id = intval($_GET['id']);
        $planification = $entityManager->getRepository('Planifications')->findOneBy( array('id' => $planification_id));
?>
    <div class="container mt-5">
      <h2><?php echo $planification->getJob()->getName();?></h2>
      <?php 
      echo (!is_null($planification->getNextTerm()) ? '<h5>' . $planifications_page_column_next_term . ' ' . $planification->getNextTerm()->format('d/m/Y H:i:s') . '</h5>' : ''); ?>
      <ul class="nav nav-tabs mt-3">
        <li class="nav-item">
          <a class="nav-link active" data-toggle="tab" href="#infos"><?php echo $planification_page_infos_tab; ?></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="#parameters"><?php echo $planification_page_parameters_tab; ?></a>
        </li>
      </ul>
      <div id="myTabContent" class="tab-content mt-3 w-75">
        <div class="tab-pane fade show active" id="infos">
          <ul class="list-group">
            <li class="list-group-item d-flex justify-content-between align-items-center">
              <?php echo $planifications_page_column_job ?>
              <span class="bg-primary w-50 text-center p-1 text-white clickable hover-dark cursor-pointer" data-type="job" data-id="<?php echo $planification->getJob()->getId() ?>"><?php echo $planification->getJob()->getName(); ?></span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
              <?php echo $planification_page_run_by ?>
              <span class="bg-primary w-50 text-center p-1 text-white"><?php echo $planification->getUser()->getName(); ?></span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
              <?php echo $planifications_page_column_start ?>
              <span class="bg-primary w-50 text-center p-1 text-white"><?php echo $planification->getStartDate()->format('d/m/Y H:i:s'); ?></span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
              <?php echo $planifications_page_column_end ?>
              <?php echo is_null($planification->getEndDate()) ? '' : '<span class="bg-primary w-50 text-center p-1 text-white">' . $planification->getEndDate()->format('d/m/Y H:i:s') . '</span>' ?>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
              <?php echo $planifications_page_column_frequency ?>
              <?php echo (!is_null($planification->getFrequencyDay())? '<span class="bg-primary w-50 text-center p-1 text-white">' . (!is_null($planification->getFrequencyTime()) && !is_null($planification->getFrequencyDay()) ? (($planification->getFrequencyDay() == 0 ? '' : $planification->getFrequencyDay()) . ' ' . $planifications_page_days . ' ' . $planification->getFrequencyTime()->format('H:i:s')) : '') . '</span>':''); ?>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
              <?php echo $planifications_page_column_status ?>
              <span class="bg-primary w-50 text-center p-1 text-white"><?php echo $status_traduction_planification[$planification->getStatus()]; ?></span>
            </li>
          </ul>
        </div>
        <div class="tab-pane fade show" id="parameters"><?php/************************-***************/?>
          <?php
            if($planification->getParams() == '{}'){// no parameter for this job
              echo $planification_page_no_parameter;
            } else {// at leat one parameter for this job
              echo '          <ul class="list-group">' . "\n";
              $params = json_decode($planification->getParams());
              foreach($params as $key => $value){
                if(!in_array($key, ['PlanificationStartDate', 'PlanificationFrequency', 'PlanificationEndDate'])){
                  echo '                <li class="list-group-item d-flex justify-content-between align-items-center">
              ' . $key . '
              <span class="bg-primary w-50 text-center p-1 text-white">' . $value . '</span>
            </li>';
                }                
              }
              echo '          </ul>' . "\n";
            }
          ?>
        </div>
      </div>
    </div>
<?php include '../element/footer.php'; ?>
