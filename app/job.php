<?php 
  include '../config/config.php';
  include '../lang/' . $lang . '.php';
  $current = 'job';
?>
<?php include '../element/header.php'; ?>
<?php
        include '../tools/utils.php';
        $job_id = intval($_GET['id']);
        $job = $entityManager->getRepository('Jobs')->findOneBy( array('id' => $job_id));
        $executions = $entityManager->getRepository('Executions')->findBy( array('job' => $job), array('startDate' => 'DESC'));
        $jobparams = $entityManager->getRepository('JobParams')->findBy( array('job' => $job), array('id' => 'ASC'));
        $parents = $entityManager->getRepository('ParentJobs')->findBy( array('child' => $job), array('id' => 'ASC'));
        $children = $entityManager->getRepository('ParentJobs')->findBy( array('parent' => $job), array('id' => 'ASC'));
?>
    <div class="container mt-5">
      <h2><?php echo $job->getName(); ?></h2>
      <ul class="nav nav-tabs mt-3">
        <li class="nav-item">
          <a class="nav-link active" data-toggle="tab" href="#infos"><?php echo $job_page_infos_tab; ?></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="#run"><?php echo $job_page_run_tab; ?></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="#executions"><?php echo $job_page_executions_tab; ?></a>
        </li>
      </ul>
      <div id="myTabContent" class="tab-content mt-3">
        <div class="tab-pane fade show active" id="infos">
          <p><?php echo $job->getDescription(); ?></p>
          <div class="card border-primary mb-3 w-50">
            <div class="card-body">
              <p class="card-text"><?php echo $job_page_group; ?>
                <?php
                  if(is_null($job->getGroup())){
                    echo '                <span class="float-right">' . $job_page_no_group . '</span>';
                  } else {
                    echo '                <span class="rounded text-white bg-primary pr-2 pl-2 float-right clickable cursor-pointer hover-dark" data-type="group" data-id="' . $job->getGroup()->getId() . '">' . $job->getGroup()->getName() . '</span>';
                  }
                ?>
              </p>
              <p class="card-text d-table w-100"><?php echo $job_page_parents; ?>
                  <?php
                    if(count($parents) == 0){
                      echo '<span class="float-right">' . $job_page_no_parent . '</span>';
                    } else {
                      echo '                  <select multiple="" class="form-control float-right w-50">' . "\n";
                      foreach($parents as $parent){
                        echo '                    <option class="clickable cursor-pointer" data-type="job" data-id="' . $parent->getParent()->getId() . '" value="' . $parent->getParent()->getName() . '">' . $parent->getParent()->getName() . "</option>\n";
                      }
                      echo '                  </select>' . "\n";
                    }
                  ?>
              </p>
              <p class="card-text d-table w-100"><?php echo $job_page_childs; ?>
                  <?php
                    if(count($children) == 0){
                      echo '<span class="float-right">' . $job_page_no_child . '</span>';
                    } else {
                      echo '                  <select multiple="" class="form-control float-right w-50">' . "\n";
                      foreach($children as $child){
                        echo '                    <option class="clickable cursor-pointer" data-type="job" data-id="' . $child->getChild()->getId() . '" value="' . $child->getChild()->getName() . '">' . $child->getChild()->getName() . "</option>\n";
                      }
                      echo '                  </select>' . "\n";
                    }
                  ?>
              </p>
            </div>
          </div>
        </div>
        <div class="tab-pane fade col-lg-6" id="run">
          <?php
            echo '          <form action="run.php">
                              <input type="hidden" name="id" value="' . $job_id . '">
            <fieldset>
              <legend>' . $job_page_run_job . '</legend>' . "\n";
            echo  '              <button class="d-block btn btn-primary mb-3" type="button" data-toggle="collapse" data-target="#planificationBlock" aria-expanded="false" aria-controls="collapseExample">
              ' . $job_page_planification_button . '
              </button>
              <div class="collapse card border-primary mb-3" id="planificationBlock">
                <div class="card-body pt1">
                <strong class="text-white">' . $job_page_planification_title . '</strong><br>
                <label class="mt-1">' . $job_page_planification_start_date_label . '</label>
                <input type="datetime-local" class="form-control" name="PlanificationStartDate" value="">
                <label class="mt-1">Fréquence</label>
                <input type="text" class="form-control" name="PlanificationFrequency" value=""placeholder="hh:mm" pattern="[0-9]+:[0-9]{2}">
                <label class="mt-1">Date Fin</label>
                <input type="datetime-local" class="form-control" name="PlanificationEndDate" value="">' . "\n";
            
            echo '                </div>
              </div>' . "\n";
            echo '              <script>
                                  document.querySelector("input[name=PlanificationFrequency]").onchange = 
                                  document.querySelector("input[name=PlanificationEndDate]").onchange = function() { 
                                    var startDate = document.querySelector("input[name=PlanificationStartDate]");
                                    var frequency = document.querySelector("input[name=PlanificationFrequency]");
                                    var endDate = document.querySelector("input[name=PlanificationEndDate]");
                                    if(frequency.value != "" || endDate.value != "") {
                                      startDate.setAttribute("required", "required");
                                      if(startDate.value == ""){
                                        startDate.classList.add("bg-danger");
                                      }
                                    } else {
                                      startDate.removeAttribute("required");
                                      startDate.classList.remove("bg-danger");
                                    }
                                    var freqPattern = /[0-9]+:[0-9]+{2}/;
                                    if(frequency.value != "" && !freqPattern.test(frequency.value)){
                                      frequency.classList.add("bg-danger");
                                    } else {                                    
                                      frequency.classList.remove("bg-danger");
                                    }
                                  }
                                </script>' . "\n";

                if(count($children) == 0){ //Current job is not a job parent
                  foreach($jobparams as $jobparam){
                    if($jobparam->getParam()->getType()->getName() != 'select'){
                      echo '              <div class="form-group">
                <label>' . $jobparam->getParam()->getName() . '</label>
                <input type="' . $jobparam->getParam()->getType()->getName() . '" class="form-control" aria-describedby="Help' . $jobparam->getParam()->getId() . '" name="' . $jobparam->getParam()->getName() . '" value="' . $jobparam->getDefaultValue() . '">
                <small id="Help' . $jobparam->getParam()->getId() . '" class="form-text text-muted">' . $jobparam->getParam()->getDescription() . '</small>
              </div>';
                    } else {
                      $valuelists = $entityManager->getRepository('ValueLists')->findBy( array('param' => $jobparam->getParam()), array('id' => 'ASC'));
                      echo '              <div class="form-group">
                <label>' . $jobparam->getParam()->getName() . '</label>
                <select class="form-control" aria-describedby="Help' . $jobparam->getParam()->getId() . '" name="' . $jobparam->getParam()->getName() . '">' . "\n";
                      foreach($valuelists as $valuelist){
                        echo '                <option value="' . $valuelist->getValue() . '" ' . ($jobparam->getDefaultValue()==$valuelist->getValue()? 'selected' : '') . '>' . $valuelist->getValue() . "</option>\n";
                      }
                      echo '                </select>
                <small id="Help' . $jobparam->getParam()->getId() . '" class="form-text text-muted">' . $jobparam->getParam()->getDescription() . '</small>
              </div>' . "\n";
                    }                
                  }
                } else { //Current job is a job parent
                  echo '            <input type="hidden" " name="multi" value="true">' . "\n";
                  foreach($children as $child){
                    echo '              <hr class="bg-primary">' . "\n";
                    echo '              <h5>' . $child->getChild()->getName() . '</h5>' . "\n";
                    $jobparams = $entityManager->getRepository('JobParams')->findBy( array('job' => $child->getChild()), array('id' => 'ASC'));
                    foreach($jobparams as $jobparam){
                      if($jobparam->getParam()->getType()->getName() != 'select'){
                        echo '              <div class="form-group">
                <label>' . $jobparam->getParam()->getName() . '</label>
                <input type="' . $jobparam->getParam()->getType()->getName() . '" class="form-control" aria-describedby="Help' . $jobparam->getParam()->getId() . '" name="' . $child->getChild()->getId() . '_' . $jobparam->getParam()->getName() . '" value="' . $jobparam->getDefaultValue() . '">
                <small id="Help' . $jobparam->getParam()->getId() . '" class="form-text text-muted">' . $jobparam->getParam()->getDescription() . '</small>
              </div>';
                      } else {
                        $valuelists = $entityManager->getRepository('ValueLists')->findBy( array('param' => $jobparam->getParam()), array('id' => 'ASC'));
                        echo '              <div class="form-group">
                <label>' . $jobparam->getParam()->getName() . '</label>
                <select class="form-control" aria-describedby="Help' . $jobparam->getParam()->getId() . '" name="' . $child->getChild()->getId() . '_' . $jobparam->getParam()->getName() . '">' . "\n";
                        foreach($valuelists as $valuelist){
                          echo '                <option value="' . $valuelist->getValue() . '" ' . ($jobparam->getDefaultValue()==$valuelist->getValue()? 'selected' : '') . '>' . $valuelist->getValue() . "</option>\n";
                        }
                        echo '                </select>
                <small id="Help' . $jobparam->getParam()->getId() . '" class="form-text text-muted">' . $jobparam->getParam()->getDescription() . '</small>
              </div>' . "\n";
                      }                
                    }
                  }
                }
            echo '              <button type="submit" class="btn btn-primary">' . $job_page_submit_button . '</button>
            </fieldset>
          </form>'  . "\n";;
          ?>
        </div>
        <div class="tab-pane fade" id="executions">
          <?php
            if(count($executions) == 0) {
              echo $executions_page_empty;
            } else {
              echo '
                              <div id="periodSelection" class="ml-5 mt-5 float-left">
                              <button id="semaine" class="btn btn-primary d-block mt-5 ml-5 w-75">' . $job_page_executions_7_days . '</button>
                              <button id="mois" class="btn btn-primary d-block mt-2 ml-5 w-75">' . $job_page_executions_30_days . '</button>
                              <button id="an" class="btn btn-primary d-block mt-2 ml-5 w-75">' . $job_page_executions_12_months . '</button>
                              <button id="total" class="btn btn-primary d-block mt-2 ml-5 w-75">' . $job_page_executions_total . '</button>
                              </div>
                              <div class="float-right w-75 mr-0">
                                <canvas id="canvas"></canvas>
                              </div>';
              echo '          <table class="table table-hover">
            <thead>
              <tr>
                <th scope="col">' . $executions_page_column_start . '</th>
                <th scope="col">' . $executions_page_column_end . '</th>
                <th scope="col">' . $executions_page_column_duration . '</th>
                <th scope="col">' . $executions_page_column_status . '</th>
              </tr>
            </thead>
            <tbody>' . "\n";
              $cpt = 0;
              foreach($executions as $execution){
                echo '              <tr class="' . ($cpt % 2 == 0 ? 'table-light' : 'table-dark') . ' clickable cursor-pointer" data-type="execution" data-id="' . $execution->getId() . '">
                <td scope="row">' . $execution->getStartDate()->format('d/m/Y H:i:s') . '</th>
                <td>' . (is_null($execution->getEndDate())?$job_page_running_exec:$execution->getEndDate()->format('d/m/Y H:i:s')) . ($execution->getStatus() == -1 ? ' <img src="../img/progress.gif" class="h1-5em float-right">':'') .  '</td>
                <td>' . (!is_null($execution->getEndDate())? $execution->getStartDate()->diff($execution->getEndDate())->format('%H h %I m %S s'):'') . '</td>
                <td>' . $status_traduction[$execution->getStatus()] . '</td>
              </tr>' . "\n";
                $cpt++;
              }
              echo '              </tbody>
            </table>' . "\n";
            }            
          ?>
        </div>
      </div>
    </div>
    <script>
      <?php
        // Executions from the start
        $executions = $entityManager->getRepository('Executions')->findBy( array('job' => $job), array('startDate' => 'ASC'));
        $totalArray = [];
        $successTotalArray = [];
        $failTotalArray = [];
        $runningTotalArray = [];
        if(count($executions) != 0) {
          foreach($executions as $execution){
            if(!array_key_exists($execution->getStartDate()->format('Y'), $totalArray)){
              $totalArray[$execution->getStartDate()->format('Y')] = 1;
              if($execution->getStatus() == 1){
                $successTotalArray[$execution->getStartDate()->format('Y')] = 1;
              } elseif($execution->getStatus() == 0){
                $failTotalArray[$execution->getStartDate()->format('Y')] = 1;
              } else {
                $runningTotalArray[$execution->getStartDate()->format('Y')] = 1;
              }
            } else {
              $totalArray[$execution->getStartDate()->format('Y')] = $totalArray[$execution->getStartDate()->format('Y')] + 1;
              if($execution->getStatus() == 1){
                if(!array_key_exists($execution->getStartDate()->format('Y'), $successTotalArray)){
                  $successTotalArray[$execution->getStartDate()->format('Y')] = 1;
                } else {
                  $successTotalArray[$execution->getStartDate()->format('Y')] = $successTotalArray[$execution->getStartDate()->format('Y')] + 1;
                }
              } elseif($execution->getStatus() == 0){
                if(!array_key_exists($execution->getStartDate()->format('Y'), $failTotalArray)){
                  $failTotalArray[$execution->getStartDate()->format('Y')] = 1;
                } else {
                  $failTotalArray[$execution->getStartDate()->format('Y')] = $failTotalArray[$execution->getStartDate()->format('Y')] + 1;
                }
              } else {
                if(!array_key_exists($execution->getStartDate()->format('Y'), $runningTotalArray)){
                  $runningTotalArray[$execution->getStartDate()->format('Y')] = 1;
                } else {
                  $runningTotalArray[$execution->getStartDate()->format('Y')] = $runningTotalArray[$execution->getStartDate()->format('Y')] + 1;
                }
              }
            }
          }
          echo '      var periodTotal = [];';
          echo '      var successTotal = [];';
          echo '      var failTotal = [];';
          echo '      var runningTotal = [];';
          $startYear = array_keys($totalArray)[0];
          $endYear = array_keys($totalArray)[count($totalArray)-1];
          for($i=$startYear; $i<=$endYear; $i++){
            $key = str_pad($i, 4, "0", STR_PAD_LEFT);
            echo '      periodTotal.push("' . $key . '");';
            if(array_key_exists($key, $successTotalArray)){
              echo '      successTotal.push("' . $successTotalArray[$key] . '");';
            } else {
              echo '      successTotal.push("0");';
            }
            if(array_key_exists($key, $failTotalArray)){
              echo '      failTotal.push("' . $failTotalArray[$key] . '");';
            } else {
              echo '      failTotal.push("0");';
            }
            if(array_key_exists($key, $runningTotalArray)){
              echo '      runningTotal.push("' . $runningTotalArray[$key] . '");';
            } else {
              echo '      runningTotal.push("0");';
            }
          }
        } else {
          echo '      periodTotal = [];';
          echo '      successTotal = [];';
          echo '      failTotal = [];';
          echo '      runningTotal = [];';
        }

        //Executions over a year
        $today = new Datetime();
        $todayLess7Days = DateTime::createFromFormat('Y-m-d H:i:s', $today->format('Y-m-d') . ' 00:00:00');
        $todayLess7Days->sub(new DateInterval('P7D'));
        $todayLess30Days = DateTime::createFromFormat('Y-m-d H:i:s', $today->format('Y-m-d') . ' 00:00:00');
        $todayLess30Days->sub(new DateInterval('P30D'));


        $todayMonth = $today->format('m');
        $todayYear = $today->format('Y');
        $todayBeginMonth = DateTime::createFromFormat('Y-m-d H:i:s', $todayYear . '-' . $todayMonth . '-01 00:00:00');
        $todayLess1Year = $todayBeginMonth;
        $todayLess1Year->sub(new DateInterval('P1Y'));
         
        $executions = $entityManager->getRepository('Executions')->findByIdAndDate($job, $todayLess1Year);
        $yearArray = [];
        $successYearArray = [];
        $failYearArray = [];
        $runningYearArray = [];
        if(count($executions) != 0) {
          foreach($executions as $execution){
            if(!array_key_exists($execution->getStartDate()->format('m-Y'), $yearArray)){
              $yearArray[$execution->getStartDate()->format('m-Y')] = 1;
              if($execution->getStatus() == 1){
                $successYearArray[$execution->getStartDate()->format('m-Y')] = 1;
              } elseif($execution->getStatus() == 0){
                $failYearArray[$execution->getStartDate()->format('m-Y')] = 1;
              } else {
                $runningYearArray[$execution->getStartDate()->format('m-Y')] = 1;
              }
            } else {
              $yearArray[$execution->getStartDate()->format('m-Y')] = $yearArray[$execution->getStartDate()->format('m-Y')] + 1;
              if($execution->getStatus() == 1){
                if(!array_key_exists($execution->getStartDate()->format('m-Y'), $successYearArray)){
                  $successYearArray[$execution->getStartDate()->format('m-Y')] = 1;
                } else {
                  $successYearArray[$execution->getStartDate()->format('m-Y')] = $successYearArray[$execution->getStartDate()->format('m-Y')] + 1;
                }                
              } elseif($execution->getStatus() == 0){
                if(!array_key_exists($execution->getStartDate()->format('m-Y'), $failYearArray)){
                  $failYearArray[$execution->getStartDate()->format('m-Y')] = 1;
                } else {
                  $failYearArray[$execution->getStartDate()->format('m-Y')] = $failYearArray[$execution->getStartDate()->format('m-Y')] + 1;
                }
              } else {
                if(!array_key_exists($execution->getStartDate()->format('m-Y'), $runningYearArray)){
                  $runningYearArray[$execution->getStartDate()->format('m-Y')] = 1;
                } else {
                  $runningYearArray[$execution->getStartDate()->format('m-Y')] = $runningYearArray[$execution->getStartDate()->format('m-Y')] + 1;
                }
              }
            }
          }

          echo '      var periodYear = [];';
          echo '      var successYear = [];';
          echo '      var failYear = [];';
          echo '      var runningYear = [];';

          $startTemp = explode('-', $todayLess1Year->format('d-m-Y'));//explode('-', array_keys($yearArray)[0]);
          $startMonth = $startTemp[1];
          $startYear = $startTemp[2];
          $endTemp = explode('-', (new DateTime())->format('d-m-Y'));//explode('-', array_keys($yearArray)[count($yearArray)-1]);
          $endMonth = $endTemp[1];
          $endYear = $endTemp[2];
          for($i=intval($startYear); $i<=$endYear; $i++){
            if($i == intval($startYear)){
              $debut = intval($startMonth);
            } else {
              $debut = 1;
            }
            if($i == intval($endYear)){
              $fin = intval($endMonth);
            } else {
              $fin = 12;
            }
            for($j=$debut; $j<=$fin; $j++){
              $key = str_pad($j, 2, "0", STR_PAD_LEFT) . '-' . str_pad($i, 4, "0", STR_PAD_LEFT);
              echo '      periodYear.push("' . $key . '");';
              if(array_key_exists($key, $successYearArray)){
                echo '      successYear.push("' . $successYearArray[$key] . '");';
              } else {
                echo '      successYear.push("0");';
              }
              if(array_key_exists($key, $failYearArray)){
                echo '      failYear.push("' . $failYearArray[$key] . '");';
              } else {
                echo '      failYear.push("0");';
              }
              if(array_key_exists($key, $runningYearArray)){
                echo '      runningYear.push("' . $runningYearArray[$key] . '");';
              } else {
                echo '      runningYear.push("0");';
              }
            }
          }
        } else {
          echo '      periodYear = [];';
          echo '      successYear = [];';
          echo '      failYear = [];';
          echo '      runningYear = [];';
        }

        //30 days executions
        $executions = $entityManager->getRepository('Executions')->findByIdAndDate($job, $todayLess30Days);
        $monthArray = [];
        $successMonthArray = [];
        $failMonthArray = [];
        $runningMonthArray = [];
        if(count($executions) != 0) {
          foreach($executions as $execution){
            if(!array_key_exists($execution->getStartDate()->format('d-m-Y'), $monthArray)){
              $monthArray[$execution->getStartDate()->format('d-m-Y')] = 1;
              if($execution->getStatus() == 1){
                $successMonthArray[$execution->getStartDate()->format('d-m-Y')] = 1;
              } elseif($execution->getStatus() == 0){
                $failMonthArray[$execution->getStartDate()->format('d-m-Y')] = 1;
              } else {
                $runningMonthArray[$execution->getStartDate()->format('d-m-Y')] = 1;
              }
            } else {
              $monthArray[$execution->getStartDate()->format('d-m-Y')] = $monthArray[$execution->getStartDate()->format('d-m-Y')] + 1;
              if($execution->getStatus() == 1){
                if(!array_key_exists($execution->getStartDate()->format('d-m-Y'), $successMonthArray)){
                  $successMonthArray[$execution->getStartDate()->format('d-m-Y')] = 1;
                } else {
                  $successMonthArray[$execution->getStartDate()->format('d-m-Y')] = $successMonthArray[$execution->getStartDate()->format('d-m-Y')] + 1;
                }                
              } elseif($execution->getStatus() == 0){
                if(!array_key_exists($execution->getStartDate()->format('d-m-Y'), $failMonthArray)){
                  $failMonthArray[$execution->getStartDate()->format('d-m-Y')] = 1;
                } else {
                  $failMonthArray[$execution->getStartDate()->format('d-m-Y')] = $failMonthArray[$execution->getStartDate()->format('d-m-Y')] + 1;
                }
              } else {
                if(!array_key_exists($execution->getStartDate()->format('d-m-Y'), $runningMonthArray)){
                  $runningMonthArray[$execution->getStartDate()->format('d-m-Y')] = 1;
                } else {
                  $runningMonthArray[$execution->getStartDate()->format('d-m-Y')] = $runningMonthArray[$execution->getStartDate()->format('d-m-Y')] + 1;
                }
              }
            }
          }

          echo '      var periodMonth = [];';
          echo '      var successMonth = [];';
          echo '      var failMonth = [];';
          echo '      var runningMonth = [];';
          $listDays = getDatesFromRange($todayLess30Days->format('d-m-Y'), (new Datetime())->format('d-m-Y')); 

          foreach($listDays as $key){
            echo '      periodMonth.push("' . $key . '");';
            if(array_key_exists($key, $successMonthArray)){
              echo '      successMonth.push("' . $successMonthArray[$key] . '");';
            } else {
              echo '      successMonth.push("0");';
              }
            if(array_key_exists($key, $failMonthArray)){
              echo '      failMonth.push("' . $failMonthArray[$key] . '");';
            } else {
              echo '      failMonth.push("0");';
            }
            if(array_key_exists($key, $runningMonthArray)){
              echo '      runningMonth.push("' . $runningMonthArray[$key] . '");';
            } else {
              echo '      runningMonth.push("0");';
            }
          }          
        } else {
          echo '      periodMonth = [];';
          echo '      successMonth = [];';
          echo '      failMonth = [];';
          echo '      runningMonth = [];';
        }

        //7 days executions
        $executions = $entityManager->getRepository('Executions')->findByIdAndDate($job, $todayLess7Days);

        $dayArray = [];
        $successDayArray = [];
        $failDayArray = [];
        $runningDayArray = [];
        if(count($executions) != 0) {
          foreach($executions as $execution){
            if(!array_key_exists($execution->getStartDate()->format('d-m-Y'), $dayArray)){
              $dayArray[$execution->getStartDate()->format('d-m-Y')] = 1;
              if($execution->getStatus() == 1){
                $successDayArray[$execution->getStartDate()->format('d-m-Y')] = 1;
              } elseif($execution->getStatus() == 0){
                $failDayArray[$execution->getStartDate()->format('d-m-Y')] = 1;
              } else {
                $runningDayArray[$execution->getStartDate()->format('d-m-Y')] = 1;
              }
            } else {
              $dayArray[$execution->getStartDate()->format('d-m-Y')] = $dayArray[$execution->getStartDate()->format('d-m-Y')] + 1;
              if($execution->getStatus() == 1){
                if(!array_key_exists($execution->getStartDate()->format('d-m-Y'), $successDayArray)){
                  $successDayArray[$execution->getStartDate()->format('d-m-Y')] = 1;
                } else {
                  $successDayArray[$execution->getStartDate()->format('d-m-Y')] = $successDayArray[$execution->getStartDate()->format('d-m-Y')] + 1;
                }                
              } elseif($execution->getStatus() == 0){
                if(!array_key_exists($execution->getStartDate()->format('d-m-Y'), $failDayArray)){
                  $failDayArray[$execution->getStartDate()->format('d-m-Y')] = 1;
                } else {
                  $failDayArray[$execution->getStartDate()->format('d-m-Y')] = $failDayArray[$execution->getStartDate()->format('d-m-Y')] + 1;
                }
              } else {
                if(!array_key_exists($execution->getStartDate()->format('d-m-Y'), $runningDayArray)){
                  $runningDayArray[$execution->getStartDate()->format('d-m-Y')] = 1;
                } else {
                  $runningDayArray[$execution->getStartDate()->format('d-m-Y')] = $runningDayArray[$execution->getStartDate()->format('d-m-Y')] + 1;
                }
              }
            }
          }

          echo '      var periodWeek = [];';
          echo '      var successWeek = [];';
          echo '      var failWeek = [];';
          echo '      var runningWeek = [];';
          $listDays = getDatesFromRange($todayLess7Days->format('d-m-Y'), (new Datetime())->format('d-m-Y')); 

          foreach($listDays as $key){
            echo '      periodWeek.push("' . $key . '");';
            if(array_key_exists($key, $successDayArray)){
              echo '      successWeek.push("' . $successDayArray[$key] . '");';
            } else {
              echo '      successWeek.push("0");';
              }
            if(array_key_exists($key, $failDayArray)){
              echo '      failWeek.push("' . $failDayArray[$key] . '");';
            } else {
              echo '      failWeek.push("0");';
            }
            if(array_key_exists($key, $runningDayArray)){
              echo '      runningWeek.push("' . $runningDayArray[$key] . '");';
            } else {
              echo '      runningWeek.push("0");';
            }
          }          
        } else {
          echo '      periodWeek = [];';
          echo '      successWeek = [];';
          echo '      failWeek = [];';
          echo '      runningWeek = [];';
        }
        
      ?>
    </script>
    <script src="../js/job.js"></script>
    <script src="../js/Chart.min.js"></script>
    <script src="../js/utils.js"></script>
<?php include '../element/footer.php'; ?>
