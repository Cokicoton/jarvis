<?php 
  include '../config/config.php';
  include '../lang/' . $lang . '.php';
  $current = !isset($current) ? 'jobs' : $current;
?>
<?php include '../element/header.php'; ?>
    <div class="float-right display-3 text-white bg-primary mr-5 mt-3 text-center rounded-circle cursor-pointer add-button hover-dark" data-page="jobs">+</div>
    <div class="container mt-5">
      <h2 class="text-center"><?php echo $jobs_page_title; ?></h2>
      <?php
        $jobs= $entityManager->getRepository('Jobs')->findBy(array(), array('lastExec' => 'DESC'));
        if (count($jobs) == 0){ // no job in Jobs table
          echo '    <div class="">
                      ' . $jobs_page_empty . '
                    </div>' . "\n";
        } else { // at leat one job in Jobs table
          echo '    <div class="d-flex flex-wrap justify-content-around">' . "\n";
          foreach($jobs as $job){
            if(is_null($job->getGroup())){
              $color = $default_color_jobs;
            } else {
              $color = is_null($job->getGroup()->getColor())?$default_color_jobs:$job->getGroup()->getColor();
            }
            echo '        <div class="card text-white ' . $color . ' mb-3 cursor-pointer hover-dark clickable" style="max-width: 20rem; min-width: 30%" data-type="job" data-id="' . $job->getId() . '">
          <div class="card-header">' . $job->getId() . '</div>
          <div class="card-body">
            <h4 class="card-title">' . $job->getName() . '</h4>
            <p class="card-text">' . $job->getDescription() . '</p>
          </div>
        </div>';
          }
        }      
      ?>
      </div>
    </div>  
<?php include '../element/footer.php'; ?>

