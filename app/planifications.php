<?php 
  include '../config/config.php';
  include '../lang/' . $lang . '.php';
  $current = !isset($current) ? 'planifications' : $current;
?>
<?php include '../element/header.php'; ?>
<?php
  $planifications = $entityManager->getRepository('Planifications')->findBy(array(), array('startDate' => 'DESC'));
?>
    <div class="container mt-5">
      <h2><?php echo $planifications_page_title; ?></h2>
          <?php
            if(count($planifications) == 0) {// no planification in Planifications table
              echo $planifications_page_empty;
            } else {// at leat one planification in Planifications table
              echo '          <table class="table table-hover">
            <thead>
              <tr style="color:#eee">
                <th scope="col">' . $planifications_page_column_job . '</th>
                <th scope="col">' . $planifications_page_column_start . '</th>
                <th scope="col">' . $planifications_page_column_end . '</th>
                <th scope="col">' . $planifications_page_column_frequency . '</th>
                <th scope="col">' . $planifications_page_column_next_term . '</th>
                <th scope="col">' . $planifications_page_column_status . '</th>
              </tr>
            </thead>
            <tbody>' . "\n";
              $cpt = 0;
              foreach($planifications as $planification){
                echo '              <tr class="' . ($cpt % 2 == 0 ? 'table-light' : 'table-dark') . ' clickable cursor-pointer" data-type="planification" data-id="' . $planification->getId() . '">
                <td scope="row" class="font-weight-bold">' . $planification->getJob()->getName() . '</th>
                <td scope="row">' . $planification->getStartDate()->format('d/m/Y H:i:s') . '</th>
                <td>' . (!is_null($planification->getEndDate()) ? $planification->getEndDate()->format('d/m/Y H:i:s') : '') .  '</td>
                <td>' . (!is_null($planification->getFrequencyTime()) && !is_null($planification->getFrequencyDay()) ? (($planification->getFrequencyDay() == 0 ? '' : $planification->getFrequencyDay()) . ' ' . $planifications_page_days . ' ' . $planification->getFrequencyTime()->format('H:i:s')) : '') . '</td>
                <td>' . (!is_null($planification->getNextTerm()) ? $planification->getNextTerm()->format('d/m/Y H:i:s') : '') .  '</td>
                <td>' . $status_traduction_planification[$planification->getStatus()] . '</td>
              </tr>' . "\n";
                $cpt++;
              }
              echo '              </tbody>
            </table>' . "\n";
            }
          ?> 
    </div>
<?php include '../element/footer.php'; ?>

