<?php
  //Elements
  //  Nav (nav.php)
  $nav_application_name = 'Jarvis';
  $nav_home = 'Accueil';
  $nav_groups = 'Groupes';
  $nav_jobs = 'Jobs';
  $nav_executions = 'Executions';
  $nav_planifications = 'Planning';
  $nav_about = 'A propos';
  $nav_search = 'Rechercher';
  $nav_search_holder = 'Rechercher';

  //Main page (index.php)
  $main_page_title = 'Jarvis';

  //Groups page (groups.php)
  $groups_page_title = 'Groupes';
  $groups_page_empty = 'Aucun groupe existant';

  //Jobs page (jobs.php)
  $jobs_page_title = 'Jobs';
  $jobs_page_empty = 'Aucun job existant';

  //Executions page (executions.php)
  $executions_page_title = 'Executions';
  $executions_page_empty = 'Aucune exécution';
  $executions_page_column_job = 'Job';
  $executions_page_column_start = 'Début';
  $executions_page_column_end = 'Fin';
  $executions_page_column_duration = 'Durée';
  $executions_page_column_status = 'Statut';

  //Planifications page (planifications.php)
  $planifications_page_title = 'Planning';
  $planifications_page_empty = 'Aucun job programmé';
  $planifications_page_column_job = 'Job';
  $planifications_page_column_start = 'Début';
  $planifications_page_column_end = 'Fin';
  $planifications_page_column_frequency = 'Fréquence';
  $planifications_page_column_next_term = 'Prochaine exécution';
  $planifications_page_column_status = 'Statut';
  $planifications_page_days = 'jours';

  //About page (about.php)
  $about_page_title = 'A propos';
  $about_page_version = 'Version';
  $about_page_license = 'License';
  $about_page_git_repository = 'Dépôt Git';

  //Job detail page (job.php)
  $job_page_title = "Job";
  $job_page_infos_tab = 'Infos';
  $job_page_run_tab = 'Lancer';
  $job_page_executions_tab = 'Executions';
  $job_page_run_job = 'Exécuter le job';
  $job_page_submit_button = 'Executer';
  $job_page_running_exec = 'En cours';
  $job_page_group = 'Groupe:';
  $job_page_no_group = 'Aucun';
  $job_page_parents = 'Parents:';
  $job_page_no_parent = 'Aucun';
  $job_page_childs = 'Enfants:';
  $job_page_no_child = 'Aucun';
  $job_page_planification_button = 'Planifier une exécution';
  $job_page_planification_title = 'Planifiction';
  $job_page_planification_start_date_label = 'Date de début';
  $job_page_planification_frequency_label = 'Fréquence';
  $job_page_planification_end_date_label = 'Date de fin';
  $job_page_executions_7_days = '7 derniers jours';
  $job_page_executions_30_days = '30 derniers jours';
  $job_page_executions_12_months = '12 derniers mois';
  $job_page_executions_total = 'Depuis le début';

  //Execution page (execution.php)
  $execution_page_title = "Exécution";
  $execution_page_infos_tab = 'Infos';
  $execution_page_parameters_tab = 'Paramètres';
  $execution_page_logs_tab = 'Logs';
  $execution_page_run_by = 'Lancé par';
  $execution_page_no_parameter = 'Aucun paramètre';

  //Planification page (planification.php)
  $planification_page_title = "Planification";
  $planification_page_infos_tab = 'Job';
  $planification_page_parameters_tab = 'Paramètres';
  $planification_page_run_by = 'Lancé par';
  $planification_page_no_parameter = 'Aucun paramètre';

  //Group page (group.php)
  $group_page_title = "Groupe";
  $group_page_no_jobs = 'Aucun job rattaché à ce groupe';

  //Search page (search.php)
  $search_page_title = 'Résultat de la recherche';
  $search_page_no_job_found = 'Aucun job trouvé';
  $search_page_no_group_found = 'Aucun groupe trouvé';

  //Traduction tables
  $status_traduction = array(
                         -1=> 'En cours',
                         0 => 'Echec',
                         1 => 'Succès'
                       );
  $status_traduction_planification = array(
                         0 => 'Terminé',
                         1 => 'Valide'
                       );

  //New group page (new_group.php)
  $new_group_page_title = 'Ajouter un groupe';
  $new_group_page_add_job_button = "Ajouter un job";
  $new_group_page_submit_button = "Ajouter le groupe";
  $new_group_page_no_job_add = "Aucun job ajouté";
  $new_group_page_label_name_field = "Nom du groupe";
  $new_group_page_placeholder_name_field = "Nom du groupe";
  $new_group_page_label_description_field = "Description du groupe";
  $new_group_page_label_Jobs = "Jobs";
  $new_group_page_label_color_field = "Couleur";
  $new_group_page_label_order_field = "Ordre d'affichage";

  //New job page (new_job.php)
  $new_job_page_title = 'Ajouter un job';
  $new_job_page_add_parameter_button = "Ajouter un paramètre";
  $new_job_page_submit_button = "Ajouter le job";
  $new_job_page_placeholder_name_field = "Nom du job";
  $new_job_page_label_name_field = "Nom du job";
  $new_job_page_label_description_field = "Description du job";
  $new_job_page_label_Group = "Groupe";
  $new_job_page_no_group = "Aucun";
  $new_job_page_label_group_order_field = "Ordre dans le groupe";
  $new_job_page_label_script_field = "Chemin du script";
  $new_job_page_placeholder_script_field = "Chemin du script";
  $new_job_page_label_parameters = "Paramètres";
  $new_job_page_add_parameter_button = "Ajouter un paramètre";
  $new_job_page_placeholder_param_name_field = "Nom du paramètre";
  $new_job_page_placeholder_param_description_field = "Description du paramètre";
  $new_job_page_placeholder_param_default_field = 'Valeur par défaut';
  $new_job_page_modal_select_title = "Select";
  $new_job_page_modal_select_description = "Saisissez les valeurs du select (Une valeur par ligne)<br>Si vous souhaitez ajouter une description ajouter le à la fin de la ligne<br>(ex: param[desc=Description du paramètre])";
  $new_job_page_modal_select_button = "Valider";
?>
