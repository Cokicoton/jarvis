<?php
  //Elements
  //  Nav (nav.php)
  $nav_application_name = 'Jarvis';
  $nav_home = 'Home';
  $nav_groups = 'Groups';
  $nav_jobs = 'Jobs';
  $nav_executions = 'Executions';
  $nav_planifications = 'Schedule';
  $nav_about = 'About';
  $nav_search = 'Search';
  $nav_search_holder = 'Search';

  //Main page (index.php)
  $main_page_title = 'Jarvis';

  //Groups page (groups.php)
  $groups_page_title = 'Groups';
  $groups_page_empty = 'No group in database';

  //Jobs page (jobs.php)
  $jobs_page_title = 'Jobs';
  $jobs_page_empty = 'No job in database';

  //Executions page (executions.php)
  $executions_page_title = 'Executions';
  $executions_page_empty = 'No execution';
  $executions_page_column_job = 'Job';
  $executions_page_column_start = 'Start';
  $executions_page_column_end = 'End';
  $executions_page_column_duration = 'Duration';
  $executions_page_column_status = 'Status';
  //Planifications page (planifications.php)
  $planifications_page_title = 'Scheduling';
  $planifications_page_empty = 'No scheduled job';
  $planifications_page_column_job = 'Job';
  $planifications_page_column_start = 'Start';
  $planifications_page_column_end = 'End';
  $planifications_page_column_frequency = 'Frequency';
  $planifications_page_column_next_term = 'Next execution';
  $planifications_page_column_status = 'Status';
  $planifications_page_days = 'days';

  //About page (about.php)
  $about_page_title = 'About';
  $about_page_version = 'Version';
  $about_page_license = 'License';
  $about_page_git_repository = 'Git repository';

  //Job detail page (job.php)
  $job_page_title = "Job";
  $job_page_infos_tab = 'Infos';
  $job_page_run_tab = 'Run';
  $job_page_executions_tab = 'Executions';
  $job_page_run_job = 'Run the job';
  $job_page_submit_button = 'Run';
  $job_page_running_exec = 'Job is running';
  $job_page_group = 'Group:';
  $job_page_no_group = 'No';
  $job_page_parents = 'Parents:';
  $job_page_no_parent = 'No';
  $job_page_childs = 'Children:';
  $job_page_no_child = 'No';
  $job_page_planification_button = 'Schedule a run';
  $job_page_planification_title = 'Scheduling';
  $job_page_planification_start_date_label = 'Start date';
  $job_page_planification_frequency_label = 'Frequency';
  $job_page_planification_end_date_label = 'End date';
  $job_page_executions_7_days = 'Last 7 days';
  $job_page_executions_30_days = 'Last 30 days';
  $job_page_executions_12_months = 'Last 12 months';
  $job_page_executions_total = 'From the beginning';

  //Execution page (execution.php)
  $execution_page_title = "Execution";
  $execution_page_infos_tab = 'Infos';
  $execution_page_parameters_tab = 'Parameters';
  $execution_page_logs_tab = 'Logs';
  $execution_page_run_by = 'Run by';
  $execution_page_no_parameter = 'No parameter';

  //Planification page (planification.php)
  $planification_page_title = "Schedule";
  $planification_page_infos_tab = 'Job';
  $planification_page_parameters_tab = 'Parameters';
  $planification_page_run_by = 'Run by';
  $planification_page_no_parameter = 'No parameter';

  //Group page (group.php)
  $group_page_title = "Group";
  $group_page_no_jobs = 'No job for this group';

  //Search page (search.php)
  $search_page_title = 'Search results';
  $search_page_no_job_found = 'No job found';
  $search_page_no_group_found = 'No group found';

  //Traduction tables
  $status_traduction = array(
                         -1=> 'In progress',
                         0 => 'Failed',
                         1 => 'Success'
                       );
  $status_traduction_planification = array(
                         0 => 'Finished',
                         1 => 'Enabled'
                       );

  //New group page (new_group.php)
  $new_group_page_title = 'New group';
  $new_group_page_add_job_button = "Add job";
  $new_group_page_submit_button = "Add group";
  $new_group_page_no_job_add = "No job";
  $new_group_page_label_name_field = "Group name";
  $new_group_page_placeholder_name_field = "Group name";
  $new_group_page_label_description_field = "Group description";
  $new_group_page_label_Jobs = "Jobs";
  $new_group_page_label_color_field = "Color";
  $new_group_page_label_order_field = "Order of appearance";

  //New job page (new_job.php)
  $new_job_page_title = 'New job';
  $new_job_page_add_parameter_button = "Add parameter";
  $new_job_page_submit_button = "Add job";
  $new_job_page_placeholder_name_field = "Job name";
  $new_job_page_label_name_field = "Job name";
  $new_job_page_label_description_field = "Job description";
  $new_job_page_label_Group = "Group";
  $new_job_page_no_group = "None";
  $new_job_page_label_group_order_field = "Group order";
  $new_job_page_label_script_field = "Script path";
  $new_job_page_placeholder_script_field = "Script path";
  $new_job_page_label_parameters = "Parameters";
  $new_job_page_add_parameter_button = "Add parameter";
  $new_job_page_placeholder_param_name_field = "Parameter name";
  $new_job_page_placeholder_param_description_field = "Parameter description";
  $new_job_page_placeholder_param_default_field = 'Default value';
  $new_job_page_modal_select_title = "Select";
  $new_job_page_modal_select_description = "Type select values (One value by line)<br>To add a description type it at the end of the line <br>(ex: param[desc=Parameter description])";
  $new_job_page_modal_select_button = "Save";
?>
